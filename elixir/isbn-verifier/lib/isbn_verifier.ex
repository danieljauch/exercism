defmodule IsbnVerifier do
  @doc """
    Checks if a string is a valid ISBN-10 identifier

    ## Examples

      iex> ISBNVerifier.isbn?("3-598-21507-X")
      true

      iex> ISBNVerifier.isbn?("3-598-2K507-0")
      false

  """

  @digit_chars ~w|0 1 2 3 4 5 6 7 8 9|

  @spec isbn?(String.t()) :: boolean
  def isbn?(isbn) do
    formatted_isbn = normalize_isbn(isbn)

    with true <- proper_length?(formatted_isbn) do
      formatted_isbn
      |> String.codepoints()
      |> reduce_isbn()
      |> rem(11)
      |> Kernel.==(0)
    end
  end

  defp normalize_isbn(isbn) do
    String.replace(isbn, "-", "")
  end

  defp proper_length?(isbn) when length(isbn) == 10, do: true

  defp proper_length?(isbn), do: false

  defp digit_to_integer(digit_char) when digit_char in @digit_chars do
    String.to_integer(digit_char)
  end

  defp digit_to_integer(_), do: 10

  defp reduce_isbn(digit_chars, index \\ 0, acc \\ 0)

  defp reduce_isbn([], _, acc), do: acc

  defp reduce_isbn([head | tail], index, acc) do
    new_acc = digit_to_integer(head) * (10 - index) + acc

    reduce_isbn(tail, index + 1, new_acc)
  end
end
