defmodule PigLatin do
  @doc """
    Given a `phrase`, translate it a word at a time to Pig Latin.

    Words beginning with consonants should have the consonant moved to the end of
    the word, followed by "ay".

    Words beginning with vowels (aeiou) should have "ay" added to the end of the
    word.

    Some groups of letters are treated like consonants, including "ch", "qu",
    "squ", "th", "thr", and "sch".

    Some groups are treated like vowels, including "yt" and "xr".
  """
  @spec translate(phrase :: String.t()) :: String.t()
  def translate(phrase) do
    phrase
    |> String.split
    |> Enum.map(&translate_word/1)
    |> Enum.join(" ")
  end

  def translate_word(word) do
    if first_letter_vowel?(word) do
      word
      |> concaten_ay_te
    else
      start_point = word
      |> find_last_consonant

      word
      |> move_start_to_end(start_point)
      |> concaten_ay_te
    end
  end

  def first_letter_vowel?(word) do
    word
    |> String.first
    |> vowel?
  end

  def concaten_ay_te(str) do
    str <> "ay"
  end

  def move_start_to_end(word, chars_to_move \\ 1)

  def move_start_to_end(word, chars_to_move) when chars_to_move < 1 do
    word
  end

  def move_start_to_end(word, chars_to_move) do
    String.slice(word, chars_to_move..-1) <> String.slice(word, 0..chars_to_move - 1)
  end

  def find_last_consonant(word) do
    word
    |> String.codepoints
    |> last_consonant_position(0)
  end

  def last_consonant_position(word, index \\ 0)

  def last_consonant_position(word, index) when index < length(word) do
    current_letter = Enum.at(word, index)
    next_letter = Enum.at(word, index + 1)
    letter_diad = current_letter <> next_letter

    cond do
      letter_diad == "qu" ->
        last_consonant_position(word, index + 2)
      letter_diad == "xr" || letter_diad == "xb" ||
      (current_letter == "y" && !vowel?(next_letter)) ->
        0
      !vowel?(current_letter) ->
        last_consonant_position(word, index + 1)
      true ->
        index
    end
  end

  def last_consonant_position(_word, _index) do
    1
  end

  def vowel?(letter) do
    letter in ["a", "e", "i", "o", "u"]
  end
end
