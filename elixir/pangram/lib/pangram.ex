defmodule Pangram do
  @doc """
  Determines if a word or sentence is a pangram.
  A pangram is a sentence using every letter of the alphabet at least once.

  Returns a boolean.

    ## Examples

      iex> Pangram.pangram?("the quick brown fox jumps over the lazy dog")
      true

  """
  @sentence_length 26

  @spec pangram?(String.t()) :: boolean
  def pangram?(sentence) do
    with true <- String.length(sentence) >= @sentence_length do
      letter_count =
        sentence
        |> String.downcase()
        |> String.replace(~r/[^a-z]/, "")
        |> String.codepoints()
        |> MapSet.new()
        |> MapSet.size()

      letter_count == @sentence_length
    end
  end
end
