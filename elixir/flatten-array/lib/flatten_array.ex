defmodule FlattenArray do
  @doc """
    Accept a list and return the list flattened without nil values.

    ## Examples

      iex> FlattenArray.flatten([1, [2], 3, nil])
      [1,2,3]

      iex> FlattenArray.flatten([nil, nil])
      []

  """

  @spec flatten(list) :: list
  def flatten(list) do
    list
    |> do_flatten()
    |> Enum.reverse()
  end

  defp do_flatten(list, acc \\ [])

  defp do_flatten([nil | tail], acc), do: do_flatten(tail, acc)

  defp do_flatten([head | tail], acc) when is_list(head) do
    new_acc =
      head
      |> do_flatten()
      |> Kernel.++(acc)

    do_flatten(tail, new_acc)
  end

  defp do_flatten([h | t], acc), do: do_flatten(t, [h | acc])

  defp do_flatten([], acc), do: acc
end
