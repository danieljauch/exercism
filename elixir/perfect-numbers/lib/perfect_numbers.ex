defmodule PerfectNumbers do
  @doc """
  Determine the aliquot sum of the given `number`, by summing all the factors
  of `number`, aside from `number` itself.

  Based on this sum, classify the number as:

  :perfect if the aliquot sum is equal to `number`
  :abundant if the aliquot sum is greater than `number`
  :deficient if the aliquot sum is less than `number`
  """
  @spec classify(number :: integer) :: {:ok, atom} | {:error, String.t()}
  def classify(number) when number < 1 do
    {:error, "Classification is only possible for natural numbers."}
  end

  def classify(number) do
    number
    |> get_root_range()
    |> Enum.filter(&is_factor?(&1, number))
    |> Enum.flat_map(&find_compliment(&1, number))
    |> Enum.sum()
    |> Kernel.-(number)
    |> do_classify(number)
  end

  defp get_root_range(number) do
    end_of_range =
      number
      |> :math.sqrt()
      |> trunc()

    1..end_of_range
  end

  defp is_factor?(potential_factor, number), do: rem(number, potential_factor) == 0

  defp find_compliment(factor, number) do
    compliment = div(number, factor)

    case compliment do
      ^factor -> [factor]
      _ -> [factor, compliment]
    end
  end

  defp do_classify(number, number), do: {:ok, :perfect}
  defp do_classify(factor_sum, number) when factor_sum > number, do: {:ok, :abundant}
  defp do_classify(factor_sum, number) when factor_sum < number, do: {:ok, :deficient}
end
