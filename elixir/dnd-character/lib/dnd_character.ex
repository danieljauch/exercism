defmodule DndCharacter do
  alias __MODULE__

  @type t :: %__MODULE__{
          strength: pos_integer(),
          dexterity: pos_integer(),
          constitution: pos_integer(),
          intelligence: pos_integer(),
          wisdom: pos_integer(),
          charisma: pos_integer(),
          hitpoints: pos_integer()
        }

  defstruct ~w[strength dexterity constitution intelligence wisdom charisma hitpoints]a

  @spec modifier(pos_integer()) :: integer()
  def modifier(score) do
    score
    |> div(2)
    |> Kernel.-(5)
  end

  @spec ability :: pos_integer()
  def ability do
    roll_ability_dice()
  end

  @spec character :: t()
  def character do
    make_modifiers()
    |> update_hitpoints()
  end

  defp roll_die do
    Enum.random(1..6)
  end

  defp roll_ability_dice do
    1..4
    |> Enum.map(fn _ -> roll_die() end)
    |> Enum.sort()
    |> Enum.drop(1)
    |> Enum.sum()
  end

  defp make_modifiers() do
    %DndCharacter{
      strength: roll_ability_dice(),
      dexterity: roll_ability_dice(),
      constitution: roll_ability_dice(),
      intelligence: roll_ability_dice(),
      wisdom: roll_ability_dice(),
      charisma: roll_ability_dice(),
      hitpoints: nil
    }
  end

  defp update_hitpoints(%DndCharacter{} = character) do
    %{character | hitpoints: calculate_hitpoints(character)}
  end

  defp calculate_hitpoints(%DndCharacter{constitution: constitution}) do
    constitution
    |> modifier()
    |> Kernel.+(10)
  end
end
