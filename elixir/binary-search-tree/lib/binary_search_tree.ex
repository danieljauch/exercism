defmodule BinarySearchTree do
  @type bst_node :: %{data: any, left: bst_node | nil, right: bst_node | nil}

  @doc """
  Create a new Binary Search Tree with root's value as the given 'data'
  """
  @spec new(any) :: bst_node
  def new(data) do
    %{data: data, left: nil, right: nil}
  end

  @doc """
  Creates and inserts a node with its value as 'data' into the tree.
  """
  @spec insert(bst_node, any) :: bst_node
  def insert(tree, data) do
    path = get_insert_path(tree, data)

    put_in(tree, path, new(data))
  end

  defp get_insert_path(tree, data, path \\ [])

  defp get_insert_path(%{data: tree_data, left: nil}, data, path) when data <= tree_data do
    Enum.reverse([:left | path])
  end

  defp get_insert_path(%{data: tree_data, right: nil}, data, path) when data > tree_data do
    Enum.reverse([:right | path])
  end

  defp get_insert_path(%{data: tree_data, left: left_tree}, data, path) when data <= tree_data do
    get_insert_path(left_tree, data, [:left | path])
  end

  defp get_insert_path(%{data: tree_data, right: right_tree}, data, path) when data > tree_data do
    get_insert_path(right_tree, data, [:right | path])
  end

  @doc """
  Traverses the Binary Search Tree in order and returns a list of each node's data.
  """
  @spec in_order(bst_node) :: [any]
  def in_order(tree) do
    tree
    |> reduce_tree()
    |> List.flatten()
    |> Enum.reverse()
  end

  defp reduce_tree(nil), do: []

  defp reduce_tree(%{data: data, left: left, right: right}) do
    [reduce_tree(right) | [[data] | reduce_tree(left)]]
  end
end
