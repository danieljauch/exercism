defmodule Isogram do
  @doc """
  Determines if a word or sentence is an isogram
  """
  @spec isogram?(String.t()) :: boolean
  def isogram?(sentence) do
    sentence
    |> String.replace(~r/[^a-z]/i, "")
    |> String.codepoints()
    |> check_for_isogram()
  end

  defp check_for_isogram(codepoints, acc \\ [])
  defp check_for_isogram([], _acc), do: true

  defp check_for_isogram([head | tail], acc) do
    case head in acc do
      true -> false
      false -> check_for_isogram(tail, [head | acc])
    end
  end
end
