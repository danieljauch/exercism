defmodule Triangle do
  @type kind :: :equilateral | :isosceles | :scalene

  @doc """
  Return the kind of triangle of a triangle with 'a', 'b' and 'c' as lengths.
  """
  @spec kind(number, number, number) :: {:ok, kind} | {:error, String.t()}
  def kind(side, _, _) when side <= 0, do: {:error, "all side lengths must be positive"}
  def kind(_, side, _) when side <= 0, do: {:error, "all side lengths must be positive"}
  def kind(_, _, side) when side <= 0, do: {:error, "all side lengths must be positive"}

  def kind(eq, eq, eq), do: {:ok, :equilateral}

  def kind(a, b, c) do
    sides = [a, b, c]

    equality =
      sides
      |> find_longest_side()
      |> passes_equality_check?(sides)

    if equality do
      inequal_kind(a, b, c)
    else
      {:error, "side lengths violate triangle inequality"}
    end
  end

  defp inequal_kind(_ineq, eq, eq), do: {:ok, :isosceles}
  defp inequal_kind(eq, _ineq, eq), do: {:ok, :isosceles}
  defp inequal_kind(eq, eq, _ineq), do: {:ok, :isosceles}

  defp inequal_kind(_, _, _), do: {:ok, :scalene}

  defp find_longest_side([a, b, c]) when a <= c and b <= c, do: c
  defp find_longest_side([a, b, c]) when a <= b and c <= b, do: b
  defp find_longest_side([a, b, c]) when b <= a and c <= a, do: a

  defp passes_equality_check?(long_side, all_sides) when length(all_sides) > 2 do
    passes_equality_check?(long_side, all_sides -- [long_side])
  end

  defp passes_equality_check?(long_side, [short1, short2]) do
    long_side < short1 + short2
  end
end
