defmodule RobotSimulator do
  @valid_directions ~w{north east south west}a
  @max_index length(@valid_directions) - 1
  @valid_instructions ~w{R L A}

  @doc """
  Create a Robot Simulator given an initial direction and position.

  Valid directions are: `:north`, `:east`, `:south`, `:west`
  """
  @spec create(direction :: atom, position :: {integer, integer}) :: any
  def create(dir \\ nil, pos \\ nil)

  def create(nil, nil) do
    %{
      direction: :north,
      position: {0, 0}
    }
  end

  def create(dir, pos) do
    validated_direction = direction(%{direction: dir})
    validated_position = position(%{position: pos})

    if validated_direction == :invalid do
      {:error, "invalid direction"}
    else
      if validated_position == :invalid do
        {:error, "invalid position"}
      else
        %{
          direction: validated_direction,
          position: validated_position
        }
      end
    end
  end

  @doc """
  Simulate the robot's movement given a string of instructions.

  Valid instructions are: "R" (turn right), "L", (turn left), and "A" (advance)
  """
  @spec simulate(robot :: any, instructions :: String.t()) :: any
  def simulate(robot, ""), do: robot

  def simulate(robot, instructions) do
    {current_instruction, rest_instructions} = String.split_at(instructions, 1)

    if current_instruction in @valid_instructions do
      robot
      |> follow_instruction(current_instruction)
      |> simulate(rest_instructions)
    else
      {:error, "invalid instruction"}
    end
  end

  defp follow_instruction(%{direction: direction, position: position}, "R") do
    %{
      direction: turn_right(direction),
      position: position
    }
  end

  defp follow_instruction(%{direction: direction, position: position}, "L") do
    %{
      direction: turn_left(direction),
      position: position
    }
  end

  defp follow_instruction(%{direction: direction, position: position}, "A") do
    %{
      direction: direction,
      position: advance(direction, position)
    }
  end

  defp follow_instruction(_robot, _invalid_instruction), do: :invalid

  defp turn_right(direction) do
    index = Enum.find_index(@valid_directions, &(&1 == direction))

    case index do
      @max_index ->
        Enum.at(@valid_directions, 0)

      _ ->
        Enum.at(@valid_directions, index + 1)
    end
  end

  defp turn_left(direction) do
    index = Enum.find_index(@valid_directions, &(&1 == direction))

    case index do
      0 ->
        Enum.at(@valid_directions, @max_index)

      _ ->
        Enum.at(@valid_directions, index - 1)
    end
  end

  defp advance(:north, {x, y}), do: {x, y + 1}
  defp advance(:east, {x, y}), do: {x + 1, y}
  defp advance(:south, {x, y}), do: {x, y - 1}
  defp advance(:west, {x, y}), do: {x - 1, y}

  @doc """
  Return the robot's direction.

  Valid directions are: `:north`, `:east`, `:south`, `:west`
  """
  @spec direction(robot :: any) :: atom
  def direction(%{direction: dir})
      when is_atom(dir) and dir in @valid_directions do
    dir
  end

  def direction(_), do: :invalid

  @doc """
  Return the robot's position.
  """
  @spec position(robot :: any) :: {integer, integer}
  def position(%{position: {_x, _y, _z}}), do: :invalid

  def position(%{position: {x, y}}) when is_integer(x) and is_integer(y) do
    {x, y}
  end

  def position(_), do: :invalid
end
