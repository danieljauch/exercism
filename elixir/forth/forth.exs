defmodule Forth do
  use GenServer

  @words ~w(+ - * /)

  @opaque evaluator :: any

  @doc """
  Create a new evaluator.
  """
  @spec new() :: evaluator
  def new() do
    {:ok, evaluator} = GenServer.start_link(__MODULE__, [])
    evaluator
  end

  @doc """
  Evaluate an input string, updating the evaluator state.
  """
  @spec eval(evaluator, String.t()) :: evaluator
  def eval(evaluator, stack) do
    GenServer.call(evaluator, {:push, stack})
  end

  @doc """
  Return the current stack as a string with the element on top of the stack
  being the rightmost element in the string.
  """
  @spec format_stack(evaluator) :: String.t()
  def format_stack(evaluator) do
    GenServer.call(evaluator, :format)
  end

  # MODULES

  defmodule StackUnderflow do
    defexception []
    def message(_), do: "stack underflow"
  end

  defmodule InvalidWord do
    defexception word: nil
    def message(e), do: "invalid word: #{inspect(e.word)}"
  end

  defmodule UnknownWord do
    defexception word: nil
    def message(e), do: "unknown word: #{inspect(e.word)}"
  end

  defmodule DivisionByZero do
    defexception []
    def message(_), do: "division by zero"
  end

  # INTERNAL BEHAVIOR

  def init([]), do: {:ok, %{stack: ""}}

  def handle_call(:format, _from, %{stack: stack} = evaluator) do
    {:reply, do_format(stack), evaluator}
  end

  def handle_call({:push, stack}, _from, evaluator) do
    {:reply, Map.put(evaluator, :stack, stack), evaluator}
  end

  def handle_cast(cmd, _from), do: {:noreply, cmd}

  defp do_format(stack) do
    stack
    |> String.split("")
    |> Enum.filter(&(Regex.match?(~r/[\d\+\-\/\*]/, &1)))
    |> Enum.join(" ")
  end
end
