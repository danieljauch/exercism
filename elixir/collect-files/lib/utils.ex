defmodule Utils do
  @directories ["a/", "b/", "c/"]

  def is_directory?(entry) do
    entry in @directories
  end

  def ls(entry) do
    case entry do
      "a/" ->
        ["b/", "a.1", "a.2", "a.3", "c/"]

      "c/" ->
        ["c.1", "c.2", "c.3"]

      _ ->
        []
    end
  end
end
