defmodule Files do
  def collect_files(entry) do
    entry
    |> Utils.ls()
    |> Enum.reduce([], fn node, acc ->
      [acc | [sort_node(node)]]
    end)
    |> List.flatten()
  end

  defp sort_node(node) do
    case Utils.is_directory?(node) do
      false -> node
      true -> collect_files(node)
    end
  end
end
