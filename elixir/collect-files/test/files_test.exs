defmodule FilesTest do
  use ExUnit.Case

  test "can do simple subdirectories" do
    assert Files.collect_files("b/") == []
  end

  test "can do the full tree" do
    assert Files.collect_files("a/") == ["a.1", "a.2", "a.3", "c.1", "c.2", "c.3"]
  end
end
