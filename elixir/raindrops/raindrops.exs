defmodule Raindrops do
  @sounds %{ 3 => "Pling", 5 => "Plang", 7 => "Plong" }

  @doc """
  Returns a string based on raindrop factors.

  - If the number contains 3 as a prime factor, output 'Pling'.
  - If the number contains 5 as a prime factor, output 'Plang'.
  - If the number contains 7 as a prime factor, output 'Plong'.
  - If the number does not contain 3, 5, or 7 as a prime factor,
    just pass the number's digits straight through.
  """
  @spec convert(pos_integer) :: String.t()
  def convert(number) do
    ret = @sounds
    |> Enum.map(fn {k, v} -> check_rem(number, k, v) end)
    |> Enum.join

    if ret == "" do
      number |> Integer.to_string
    else
      ret
    end
  end

  defp check_rem(num, rem, result) do
    if rem(num, rem) == 0 do
      result
    else
      nil
    end
  end
end
