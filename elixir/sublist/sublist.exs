defmodule Sublist do
  @doc """
  Returns whether the first list is a sublist or a superlist of the second list
  and if not whether it is equal or unequal to the second list.
  """
  @spec compare(list, list) :: atom
  def compare(a, a), do: :equal
  def compare([], b), do: :sublist
  def compare(a, []), do: :superlist

  def compare(a, b) do
    cond do
      is_within?(a, b) -> :sublist
      is_within?(b, a) -> :superlist
      :otherwise -> :unequal
    end
  end

  defp is_within?(sublist, superlist) when length(superlist) < length(sublist) do
    false
  end

  defp is_within?(sublist, superlist) do
    if Enum.take(superlist, length(sublist)) === sublist do
      true
    else
      is_within?(sublist, tl(superlist))
    end
  end
end
