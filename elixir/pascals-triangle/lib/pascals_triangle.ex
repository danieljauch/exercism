defmodule PascalsTriangle do
  @doc """
  Calculates the rows of a pascal triangle
  with the given height
  """
  @spec rows(integer) :: [[integer]]
  def rows(num) do
    [1]
    |> Stream.iterate(&next_row/1)
    |> Enum.take(num)
  end

  defp next_row(prev_row) do
    [0 | prev_row]
    |> Stream.zip(prev_row ++ [0])
    |> Enum.map(fn {left, right} -> left + right end)
  end
end
