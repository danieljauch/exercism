defmodule ArmstrongNumber do
  @moduledoc """
  Provides a way to validate whether or not a number is an Armstrong number
  """

  @spec valid?(integer) :: boolean
  def valid?(number) when number in 1..9, do: true

  def valid?(number) do
    digits = Integer.digits(number)
    power = length(digits)

    digits
    |> Enum.map(&:math.pow(&1, power))
    |> Enum.reduce(&+/2)
    |> Kernel.==(number)
  end
end
