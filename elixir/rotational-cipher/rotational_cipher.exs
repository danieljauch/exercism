defmodule RotationalCipher do
  @alpha String.split("abcdefghijklmnopqrstuvwxyz", "", trim: true)

  @doc """
  Given a plaintext and amount to shift by, return a rotated string.

  Example:
  iex> RotationalCipher.rotate("Attack at dawn", 13)
  "Nggnpx ng qnja"
  """
  @spec rotate(text :: String.t(), shift :: integer) :: String.t()
  def rotate(text, shift) do
    text
    |> String.split("", trim: true)
    |> Enum.map(fn letter -> translate(letter, shift) end)
    |> Enum.join
  end

  def translate(letter, shift) do
    if shiftable_char?(letter) do
      letter
      |> rotate_letter(shift)
    else
      letter
    end
  end

  def rotate_letter(letter, shift) do
    if upper?(letter) do
      @alpha
      |> Enum.at(shifted_index(letter, shift))
      |> String.upcase
    else
      Enum.at(@alpha, shifted_index(letter, shift))
    end
  end

  def upper?(letter) do
    letter =~ ~r/[A-Z]/
  end

  def shifted_index(letter, shift) do
    rem(find_initial_index(letter) + shift, length(@alpha))
  end

  def find_initial_index(text) do
    @alpha
    |> Enum.find_index(fn letter -> letter == String.downcase(text) end)
  end

  def shiftable_char?(letter) do
    letter =~ ~r/[a-zA-Z]/
  end
end
