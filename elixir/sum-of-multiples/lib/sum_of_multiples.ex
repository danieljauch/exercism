defmodule SumOfMultiples do
  @doc """
  Adds up all numbers from 1 to a given end number that are multiples of the factors provided.
  """
  @spec to(non_neg_integer, [non_neg_integer]) :: non_neg_integer
  def to(limit, factors) do
    limit
    |> limit_range()
    |> Enum.filter(&valid_numbers(&1, factors))
    |> Enum.sum()
  end

  defp limit_range(limit) do
    1..(limit - 1)
  end

  defp valid_numbers(number, multiples) do
    cond do
      number in multiples -> true
      is_multiple?(number, multiples) -> true
      true -> false
    end
  end

  defp is_multiple?(_number, []), do: false

  defp is_multiple?(number, [multiple | rest]) do
    if rem(number, multiple) == 0 do
      true
    else
      is_multiple?(number, rest)
    end
  end
end
