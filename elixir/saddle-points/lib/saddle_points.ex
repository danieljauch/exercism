defmodule Matrix do
  alias __MODULE__

  @type t :: %__MODULE__{}

  @row_delimiter "\n"
  @col_delimiter " "

  defstruct ~w(rows cols vals)a

  @doc """
  Convert an `input` string, with rows separated by newlines and values
  separated by single spaces, into a `Matrix` struct.
  """
  @spec from_string(input :: String.t()) :: %Matrix{}
  def from_string(input) do
    rows = String.split(input, @row_delimiter)

    cols =
      rows
      |> List.first()
      |> String.split(@col_delimiter)
      |> length()

    vals =
      input
      |> String.split(~r/\s/)
      |> Enum.map(&String.to_integer/1)

    %Matrix{
      rows: length(rows),
      cols: cols,
      vals: vals
    }
  end

  @doc """
  Given a `matrix`, return its rows as a list of lists of integers.
  """
  @spec rows(Matrix.t()) :: list(list(integer))
  def rows(%Matrix{rows: rows, vals: vals}) do
    Enum.chunk_every(vals, rows)
  end

  @doc """
  Given a `matrix`, return its columns as a list of lists of integers.
  """
  @spec columns(Matrix.t()) :: list(list(integer))
  def columns(%Matrix{cols: cols, vals: vals} = matrix) do
    val_count = length(vals)

    matrix
    |> row_index_range()
    |> Enum.map(fn row ->
      vals
      |> Enum.slice(row, val_count)
      |> Enum.take_every(cols)
    end)
  end

  defp row_index_range(%Matrix{rows: rows}) do
    0..(rows-1)
  end
end


defmodule SaddlePoints do
  @doc """
  Parses a string representation of a matrix
  to a list of rows
  """
  @spec rows(String.t()) :: [[integer]]
  def rows(str) do
    str
    |> Matrix.from_string()
    |> Matrix.rows()
  end

  @doc """
  Parses a string representation of a matrix
  to a list of columns
  """
  @spec columns(String.t()) :: [[integer]]
  def columns(str) do
    str
    |> Matrix.from_string()
    |> Matrix.columns()
  end

  @doc """
  Calculates all the saddle points from a string
  representation of a matrix
  """
  @spec saddle_points(String.t()) :: [{integer, integer}]
  def saddle_points(str) do
    str
    |> Matrix.from_string()
  end
end
