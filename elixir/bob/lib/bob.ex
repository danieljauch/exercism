defmodule Bob do
  def hey(input) do
    normal_input = normalize_input(input)

    cond do
      is_yelling?(normal_input) && is_question?(normal_input) ->
        "Calm down, I know what I'm doing!"

      is_question?(normal_input) ->
        "Sure."

      is_yelling?(normal_input) ->
        "Whoa, chill out!"

      is_silent?(normal_input) ->
        "Fine. Be that way!"

      true ->
        "Whatever."
    end
  end

  defp normalize_input(input) do
    String.replace(input, ~r/[^\p{L}\d?!]/i, "")
  end

  defp is_question?(input) do
    String.last(input) == "?"
  end

  defp is_yelling?(input) do
    alpha_input = String.replace(input, ~r/[^\p{L}]/i, "")

    not is_silent?(alpha_input) && String.upcase(alpha_input) == alpha_input
  end

  defp is_silent?(input) do
    input == ""
  end
end
