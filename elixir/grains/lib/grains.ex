defmodule Grains do
  @doc """
  Calculate two to the power of the input minus one.
  """
  @spec square(pos_integer) :: pos_integer
  def square(number) do
    with true <- number in 1..64 do
      {:ok, do_square(number)}
    else
      _ ->
        {:error, "The requested square must be between 1 and 64 (inclusive)"}
    end
  end

  defp do_square(number) do
    2
    |> :math.pow(number - 1)
    |> trunc()
  end

  @doc """
  Adds square of each number from 1 to 64.
  """
  @spec total :: pos_integer
  def total do
    summed =
      1..64
      |> Enum.map(&do_square/1)
      |> Enum.reduce(&+/2)

    {:ok, summed}
  end
end
