defmodule ListOps do
  # Please don't use any external modules (especially List or Enum) in your
  # implementation. The point of this exercise is to create these basic
  # functions yourself. You may use basic Kernel functions (like `Kernel.+/2`
  # for adding numbers), but please do not use Kernel functions for Lists like
  # `++`, `--`, `hd`, `tl`, `in`, and `length`.

  @spec count(list) :: non_neg_integer
  def count([_|t]), do: 1 + count(t)
  def count([]), do: 0

  @spec reverse(list) :: list
  def reverse(l, acc \\ [])
  def reverse([h|t], acc), do: reverse(t, [h|acc])
  def reverse([], acc), do: acc

  @spec map(list, (any -> any)) :: list
  def map(l, f, acc \\ [])
  def map([h|t], f, acc), do: map(t, f, [f.(h)|acc])
  def map([], _f, acc), do: reverse(acc)

  @spec filter(list, (any -> as_boolean(term))) :: list
  def filter(l, f, acc \\ [])
  def filter([h|t], f, acc) do
    case f.(h) do
      true ->
        filter(t, f, [h|acc])

      _ ->
        filter(t, f, acc)
    end
  end
  def filter([], _f, acc), do: reverse(acc)

  @type acc :: any
  @spec reduce(list, acc, (any, acc -> acc)) :: acc
  def reduce([h|t], acc, f), do: reduce(t, f.(h, acc), f)
  def reduce([], acc, _f), do: acc

  @spec append(list, list) :: list
  def append(a, b) do
    case count(a) do
      0 ->
        b

      1 ->
        do_append(a, b)

      _ ->
        a
        |> reverse()
        |> do_append(b)
    end
  end

  defp do_append([h|t], b), do: do_append(t, [h|b])
  defp do_append([], b), do: b

  @spec concat([[any]]) :: [any]
  def concat([h|t]), do: append(h, concat(t))
  def concat([]), do: []
end
