defmodule Garden do
  @doc """
    Accepts a string representing the arrangement of cups on a windowsill and a
    list with names of students in the class. The student names list does not
    have to be in alphabetical order.

    It decodes that string into the various gardens for each student and returns
    that information in a map.
  """

  @class ~w|
    alice bob charlie david eve
    fred ginny harriet ileana joseph
    kincaid larry maggie nate ophelia
    pete reggie sylvia tanner ursula
    victor winnie xander ynold
  |a

  @spec info(String.t(), list(atom())) :: map
  def info(plant_rows, students \\ @class)

  def info(plant_rows, students) do
    plant_rows
    |> String.split("\n")
    |> Enum.map(&for acronym <- String.codepoints(&1), do: plant_atom(acronym))
    |> Enum.map(&Enum.chunk_every(&1, 2))
    |> zip_plants(Enum.sort(students))
  end

  defp plant_atom("C"), do: :clover
  defp plant_atom("G"), do: :grass
  defp plant_atom("R"), do: :radishes
  defp plant_atom("V"), do: :violets

  defp zip_plants([row_1, row_2], students) do
    zip_plants(
      row_1,
      row_2,
      students,
      Enum.into(@class, %{}, fn student ->
        {student, {}}
      end)
    )
  end

  defp zip_plants(plants, students, acc)
  defp zip_plants([], [], _, acc), do: acc

  defp zip_plants(
         [[plant_1, plant_2] | row_1_tail],
         [[plant_3, plant_4] | row_2_tail],
         [current_student | student_tail],
         acc
       ) do
    zip_plants(
      row_1_tail,
      row_2_tail,
      student_tail,
      %{acc | current_student => {plant_1, plant_2, plant_3, plant_4}}
    )
  end
end
