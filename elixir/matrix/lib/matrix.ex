defmodule Matrix do
  alias __MODULE__

  @type t :: %__MODULE__{}
  @row_joiner "\n"
  @col_joiner " "

  defstruct ~w(rows cols vals)a

  @doc """
  Convert an `input` string, with rows separated by newlines and values
  separated by single spaces, into a `Matrix` struct.
  """
  @spec from_string(input :: String.t()) :: %Matrix{}
  def from_string(input) do
    rows = String.split(input, @row_joiner)

    cols =
      rows
      |> List.first()
      |> String.split(@col_joiner)
      |> length()

    vals =
      input
      |> String.split(~r/\s/)
      |> Enum.map(&String.to_integer/1)

    %Matrix{
      rows: length(rows),
      cols: cols,
      vals: vals
    }
  end

  @doc """
  Write the `matrix` out as a string, with rows separated by newlines and
  values separated by single spaces.
  """
  @spec to_string(Matrix.t()) :: String.t()
  def to_string(%Matrix{} = matrix) do
    matrix
    |> rows()
    |> Enum.map(&row_to_string/1)
    |> Enum.join(@row_joiner)
  end

  @doc """
  Given a `matrix`, return its rows as a list of lists of integers.
  """
  @spec rows(Matrix.t()) :: list(list(integer))
  def rows(%Matrix{rows: rows, vals: vals}) do
    Enum.chunk_every(vals, rows)
  end

  defp row_to_string(row) do
    row
    |> Enum.map(&Integer.to_string/1)
    |> Enum.join(@col_joiner)
  end

  @doc """
  Given a `matrix` and `index`, return the row at `index`.
  """
  @spec row(Matrix.t(), index :: integer) :: list(integer)
  def row(%Matrix{} = matrix, index) do
    matrix
    |> rows()
    |> Enum.at(index)
  end

  @doc """
  Given a `matrix`, return its columns as a list of lists of integers.
  """
  @spec columns(Matrix.t()) :: list(list(integer))
  def columns(%Matrix{cols: cols, vals: vals} = matrix) do
    val_count = length(vals)

    matrix
    |> row_index_range()
    |> Enum.map(fn row ->
      vals
      |> Enum.slice(row, val_count)
      |> Enum.take_every(cols)
    end)
  end

  defp row_index_range(%Matrix{rows: rows}) do
    0..(rows-1)
  end

  @doc """
  Given a `matrix` and `index`, return the column at `index`.
  """
  @spec column(Matrix.t(), index :: integer) :: list(integer)
  def column(%Matrix{} = matrix, index) do
    matrix
    |> columns()
    |> Enum.at(index)
  end
end
