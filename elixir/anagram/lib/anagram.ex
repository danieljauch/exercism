defmodule Anagram do
  @doc """
  Returns all candidates that are anagrams of, but not equal to, 'base'.
  """
  @spec match(String.t(), [String.t()]) :: [String.t()]
  def match(base, candidates) do
    base_charmap = string_to_charmap(base)

    candidates
    |> Enum.filter(fn candidate ->
      with true <- is_different_word?(base, candidate) do
        candidate
        |> string_to_charmap()
        |> Map.equal?(base_charmap)
      end
    end)
  end

  defp string_to_charmap(str) do
    str
    |> String.downcase()
    |> String.codepoints()
    |> Enum.reduce(%{}, fn character, count_map ->
      Map.update(count_map, character, 1, &(&1 + 1))
    end)
  end

  defp is_different_word?(word1, word2) do
    String.downcase(word1) != String.downcase(word2)
  end
end
