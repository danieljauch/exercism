defmodule LinkedList do
  @empty {nil, nil}

  @opaque t :: tuple()

  @doc """
  Construct a new LinkedList
  """
  @spec new() :: t
  def new(value \\ nil)
  def new(nil), do: @empty
  def new(value), do: make_node(value)

  @doc """
  Push an item onto a LinkedList
  """
  @spec push(t, any()) :: t
  def push(@empty, elem), do: make_node(elem)
  def push(list, elem), do: make_node(elem, list)

  defp make_node(value, next \\ nil)
  defp make_node(value, next), do: {value, next}

  @doc """
  Calculate the length of a LinkedList
  """
  @spec length(t) :: non_neg_integer()
  def length(@empty), do: 0
  def length({_value, nil}), do: 1

  def length(list) do
    list
    |> next()
    |> LinkedList.length()
    |> Kernel.+(1)
  end

  @doc """
  Determine if a LinkedList is empty
  """
  @spec empty?(t) :: boolean()
  def empty?(@empty), do: true
  def empty?(_), do: false

  @doc """
  Get the value of a head of the LinkedList
  """
  @spec peek(t) :: {:ok, any()} | {:error, :empty_list}
  def peek(@empty), do: {:error, :empty_list}
  def peek({value, _next}), do: {:ok, value}

  @doc """
  Get tail of a LinkedList
  """
  @spec tail(t) :: {:ok, t} | {:error, :empty_list}
  def tail(@empty), do: {:error, :empty_list}
  def tail({_value, nil}), do: {:ok, @empty}
  def tail({_value, next}), do: {:ok, next}

  @doc """
  Remove the head from a LinkedList
  """
  @spec pop(t) :: {:ok, any(), t} | {:error, :empty_list}
  def pop(@empty), do: {:error, :empty_list}
  def pop({value, nil}), do: {:ok, value, @empty}
  def pop({value, next}), do: {:ok, value, next}

  @doc """
  Construct a LinkedList from a stdlib List
  """
  @spec from_list(list()) :: t
  def from_list([]), do: @empty

  def from_list(list) do
    [head | tail] = Enum.reverse(list)

    head
    |> new()
    |> do_from_list(tail)
  end

  defp do_from_list(linked_list, []), do: linked_list

  defp do_from_list(linked_list, [head | tail]) do
    linked_list
    |> push(head)
    |> do_from_list(tail)
  end

  @doc """
  Construct a stdlib List LinkedList from a LinkedList
  """
  @spec to_list(t) :: list()
  def to_list(@empty), do: []
  def to_list({value, nil}), do: [value]
  def to_list({value, next}), do: [value | to_list(next)]

  @doc """
  Reverse a LinkedList
  """
  @spec reverse(t) :: t
  def reverse(linked_list) do
    linked_list
    |> to_list()
    |> Enum.reverse()
    |> from_list()
  end

  defp next(nil), do: nil
  defp next({_value, next}), do: next
end
