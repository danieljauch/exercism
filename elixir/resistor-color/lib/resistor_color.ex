defmodule ResistorColor do
  @colors_list ~w|black brown red orange yellow green blue violet grey white|

  @spec colors() :: list(String.t())
  def colors, do: @colors_list

  @spec code(String.t()) :: integer()
  def code(color), do: Enum.find_index(@colors_list, &(&1 == color))
end
