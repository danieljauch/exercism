defmodule Hamming do
  @doc """
  Returns number of differences between two strands of DNA, known as the Hamming Distance.

  ## Examples

  iex> Hamming.hamming_distance('AAGTCATA', 'TAGCGATC')
  {:ok, 4}
  """
  @spec hamming_distance([char], [char]) :: {:ok, non_neg_integer} | {:error, String.t()}
  def hamming_distance(strand1, strand2) when length(strand1) != length(strand2) do
    {:error, "Lists must be the same length"}
  end

  def hamming_distance(strand1, strand2) do
    {:ok, count_errors(strand1, strand2)}
  end

  defp count_errors(list1, list2, count \\ 0)
  defp count_errors([], [], count), do: count

  defp count_errors([match | tail1], [match | tail2], count) do
    count_errors(tail1, tail2, count)
  end

  defp count_errors([_mismatch1 | tail1], [_mismatch2 | tail2], count) do
    count_errors(tail1, tail2, count + 1)
  end
end
