defmodule MatchingBrackets do
  @curlies ~W|{ }|
  @squares ~W|[ ]|
  @parens ~W|( )|
  @opens ~W|{ [ (|
  @closes ~W|} ] )|

  @doc """
  Checks that all the brackets and braces in the string are matched correctly, and nested correctly
  """
  @spec check_brackets(String.t()) :: boolean
  def check_brackets(str) do
    str
    |> String.replace(~r/[^\[\]\{\}\(\)]/, "")
    |> String.split("", trim: true)
    |> do_check_brackets()
  end

  defp do_check_brackets(brackets, open_brackets \\ [], depth \\ 0)
  defp do_check_brackets([], [], 0), do: true
  defp do_check_brackets(_brackets, _open_brackets, depth) when depth < 0, do: false
  defp do_check_brackets([], _open_brackets, _depth), do: false

  defp do_check_brackets([current_bracket | tail], open_brackets, depth) do
    shape = get_shape(current_bracket)

    case get_direction(current_bracket) do
      :open ->
        do_check_brackets(tail, [shape | open_brackets], depth + 1)

      :close ->
        with true <- length(open_brackets) > 0,
             [bracket_to_close | tail_open_brackets] <- open_brackets,
             true <- bracket_to_close == shape do
          do_check_brackets(tail, tail_open_brackets, depth - 1)
        end
    end
  end

  defp get_shape(bracket) when bracket in @curlies, do: :curly
  defp get_shape(bracket) when bracket in @squares, do: :square
  defp get_shape(bracket) when bracket in @parens, do: :paren

  defp get_direction(bracket) when bracket in @opens, do: :open
  defp get_direction(bracket) when bracket in @closes, do: :close
end
