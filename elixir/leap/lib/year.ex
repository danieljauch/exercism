defmodule Year do
  @doc """
  Returns whether 'year' is a leap year.

  A leap year occurs:

  on every year that is evenly divisible by 4
    except every year that is evenly divisible by 100
      unless the year is also evenly divisible by 400
  """
  @spec leap_year?(non_neg_integer) :: boolean
  def leap_year?(year) do
    cond do
      is_multiple_of?(year, 400) -> true
      is_multiple_of?(year, 100) -> false
      is_multiple_of?(year, 4) -> true
      true -> false
    end
  end

  defp is_multiple_of?(year, factor), do: rem(year, factor) == 0
end
