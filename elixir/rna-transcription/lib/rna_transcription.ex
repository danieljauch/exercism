defmodule RnaTranscription do
  @doc """
  Transcribes a character list representing DNA nucleotides to RNA

  ## Examples

  iex> RnaTranscription.to_rna('ACTG')
  'UGAC'
  """

  @nucleotide_map %{
    ?A => ?U,
    ?C => ?G,
    ?G => ?C,
    ?T => ?A,
  }

  @spec to_rna([char]) :: [char]
  def to_rna(dna), do: Enum.map(dna, &(@nucleotide_map[&1]))
end
