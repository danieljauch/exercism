defmodule Frequency do
  @doc """
  Count letter frequency in parallel.

  Returns a map of characters to frequencies.

  The number of worker processes to use can be set with 'workers'.
  """
  @spec frequency([String.t()], pos_integer) :: map
  def frequency(texts, workers \\ 1)
  def frequency([], _workers), do: %{}

  def frequency(texts, _workers) do
    fn ->
      texts
      |> Enum.join()
      |> String.downcase()
      |> String.graphemes()
      |> to_map()
    end
    |> Task.async()
    |> Task.await()
  end

  defp to_map(char, map \\ %{})
  defp to_map([], map), do: map

  defp to_map([head | tail], map) do
    with true <- Regex.match?(~r/\p{L}/i, head) do
      updated_count =
        map
        |> Map.get(head)
        |> update_count()

      updated_map = Map.put(map, head, updated_count)

      to_map(tail, updated_map)
    else
      _ -> to_map(tail, map)
    end
  end

  defp update_count(nil), do: 1
  defp update_count(count), do: count + 1
end
