defmodule Say do
  @doc """
  Translate a positive integer into English.
  """

  @digit_names ~w(one thousand million billion)a

  @spec in_english(integer) :: {atom, String.t()}
  def in_english(number) when number < 0, do: {:error, "number is out of range"}
  def in_english(number) when number > 999_999_999_999, do: {:error, "number is out of range"}
  def in_english(0), do: {:ok, "zero"}

  def in_english(number) do
    words =
      number
      |> Integer.digits()
      |> Enum.reverse()
      |> Enum.chunk_every(3)
      |> Enum.zip(@digit_names)
      |> translate_digit_chunks()
      |> List.flatten()
      |> Enum.join(" ")
      |> String.trim()

    {:ok, words}
  end

  defp translate_digit_chunks(chunks, acc \\ [])

  defp translate_digit_chunks([], acc), do: acc

  defp translate_digit_chunks([{numbers, digit} | tail], acc) do
    translate_digit_chunks(tail, [translate_chunk(numbers, digit) | acc])
  end

  defp translate_chunk([0, 0, 0], _digit), do: []

  defp translate_chunk([one, ten, 0], digit), do: translate_chunk([one, ten], digit)

  defp translate_chunk([one, ten, hundred] = numbers, digit) do
    [
      number_to_english(hundred),
      "hundred",
      tens_and_ones(ten, one),
      digit_to_english(digit)
    ]
  end

  defp translate_chunk([one, ten] = numbers, digit) do
    [
      tens_and_ones(ten, one),
      digit_to_english(digit)
    ]
  end

  defp translate_chunk([number] = numbers, digit) do
    [
      number_to_english(number),
      digit_to_english(digit)
    ]
  end

  defp tens_and_ones(0, ones), do: number_to_english(ones)

  defp tens_and_ones(1, 0), do: "ten"
  defp tens_and_ones(1, 1), do: "eleven"
  defp tens_and_ones(1, 2), do: "twelve"
  defp tens_and_ones(1, 3), do: "thirteen"
  defp tens_and_ones(1, 4), do: "fourteen"
  defp tens_and_ones(1, 5), do: "fifteen"
  defp tens_and_ones(1, 8), do: "eighteen"

  defp tens_and_ones(1, ones) do
    "#{number_to_english(ones)}teen"
  end

  defp tens_and_ones(tens, 0), do: tens_to_english(tens)

  defp tens_and_ones(tens, ones) do
    "#{tens_to_english(tens)}-#{number_to_english(ones)}"
  end

  defp tens_to_english(2), do: "twenty"
  defp tens_to_english(3), do: "thirty"
  defp tens_to_english(4), do: "forty"
  defp tens_to_english(5), do: "fifty"
  defp tens_to_english(6), do: "sixty"
  defp tens_to_english(7), do: "seventy"
  defp tens_to_english(8), do: "eighty"
  defp tens_to_english(9), do: "ninety"

  defp number_to_english(0), do: ""
  defp number_to_english(1), do: "one"
  defp number_to_english(2), do: "two"
  defp number_to_english(3), do: "three"
  defp number_to_english(4), do: "four"
  defp number_to_english(5), do: "five"
  defp number_to_english(6), do: "six"
  defp number_to_english(7), do: "seven"
  defp number_to_english(8), do: "eight"
  defp number_to_english(9), do: "nine"

  defp digit_to_english(:one), do: ""
  defp digit_to_english(digit_atom), do: Atom.to_string(digit_atom)
end
