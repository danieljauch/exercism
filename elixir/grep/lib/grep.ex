defmodule Grep do
  @match_flags ~w(-i -v -x)
  @output_flags ~w(-l -n)

  @spec grep(String.t(), [String.t()], [String.t()]) :: String.t()
  def grep(pattern, flags, files) do
    multiple_files = length(files) > 1
    match_flags = Enum.filter(flags, &(&1 in @match_flags))
    output_flags = Enum.filter(flags, &(&1 in @output_flags))

    files
    |> process_files()
    |> match_pattern(pattern, match_flags)
    |> format_output(output_flags, multiple_files)
  end

  defp process_files(files) do
    files
    |> Enum.into([], fn filename ->
      with {:ok, file_contents} <- File.read(filename) do
        file_contents
        |> String.split("\n", trim: true)
        |> Enum.with_index(1)
        |> Enum.into([], fn {text, line_number} ->
          %{
            text: text,
            line_number: line_number,
            filename: filename
          }
        end)
      end
    end)
    |> List.flatten()
  end

  defp match_pattern(lines, pattern, flags) do
    match_type = classify_match_type(flags)

    Enum.filter(lines, &matches?(&1.text, pattern, match_type))
  end

  defp matches?(text, pattern, :insensitive_exact) do
    String.downcase(text) == String.downcase(pattern)
  end

  defp matches?(text, pattern, :exact) do
    text == pattern
  end

  defp matches?(text, pattern, :insensitive) do
    String.contains?(String.downcase(text), String.downcase(pattern))
  end

  defp matches?(text, pattern, :inverted) do
    not String.contains?(text, pattern)
  end

  defp matches?(text, pattern, :standard) do
    String.contains?(text, pattern)
  end

  defp classify_match_type(flags) do
    cond do
      "-i" in flags and "-x" in flags -> :insensitive_exact
      "-x" in flags -> :exact
      "-i" in flags -> :insensitive
      "-v" in flags -> :inverted
      :else -> :standard
    end
  end

  defp format_output(output, flags, multi_line) do
    output_type = classify_output_type(flags)

    output
    |> Enum.map(&convert_line(&1, output_type, multi_line))
    |> Enum.uniq()
    |> Enum.join()
  end

  defp convert_line(%{filename: filename}, :filename_only, _multi_line) do
    "#{filename}\n"
  end

  defp convert_line(
         %{text: text, line_number: line_number, filename: filename},
         :line_numbers,
         true
       ) do
    "#{filename}:#{line_number}:#{text}\n"
  end

  defp convert_line(%{text: text, filename: filename}, :default, true) do
    "#{filename}:#{text}\n"
  end

  defp convert_line(%{text: text, line_number: line_number}, :line_numbers, false) do
    "#{line_number}:#{text}\n"
  end

  defp convert_line(%{text: text}, :default, false) do
    "#{text}\n"
  end

  defp classify_output_type(flags) do
    cond do
      "-l" in flags -> :filename_only
      "-n" in flags -> :line_numbers
      :else -> :default
    end
  end
end
