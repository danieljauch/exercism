defmodule SimpleCipher do
  @letters ?a..?z
  @wrap_around_back ?z
  @wrap_around_front ?z - ?a + 1

  @doc """
  Given a `plaintext` and `key`, encode each character of the `plaintext` by
  shifting it by the corresponding letter in the alphabet shifted by the number
  of letters represented by the `key` character, repeating the `key` if it is
  shorter than the `plaintext`.

  For example, for the letter 'd', the alphabet is rotated to become:

  defghijklmnopqrstuvwxyzabc

  You would encode the `plaintext` by taking the current letter and mapping it
  to the letter in the same position in this rotated alphabet.

  abcdefghijklmnopqrstuvwxyz
  defghijklmnopqrstuvwxyzabc

  "a" becomes "d", "t" becomes "w", etc...

  Each letter in the `plaintext` will be encoded with the alphabet of the `key`
  character in the same position. If the `key` is shorter than the `plaintext`,
  repeat the `key`.

  Example:

  plaintext = "testing"
  key = "abc"

  The key should repeat to become the same length as the text, becoming
  "abcabca". If the key is longer than the text, only use as many letters of it
  as are necessary.
  """
  def encode(input, key) do
    use_cipher(input, key, :encode)
  end

  @doc """
  Given a `ciphertext` and `key`, decode each character of the `ciphertext` by
  finding the corresponding letter in the alphabet shifted by the number of
  letters represented by the `key` character, repeating the `key` if it is
  shorter than the `ciphertext`.

  The same rules for key length and shifted alphabets apply as in `encode/2`,
  but you will go the opposite way, so "d" becomes "a", "w" becomes "t",
  etc..., depending on how much you shift the alphabet.
  """
  def decode(input, key) do
    use_cipher(input, key, :decode)
  end

  defp use_cipher(input, key, direction) do
    input
    |> build_charlists(key)
    |> Enum.map(&shift(&1, direction))
    |> to_string()
  end

  defp build_charlists(text, key) do
    text
    |> to_charlist()
    |> Enum.zip(make_shifts(key))
  end

  defp make_shifts(key) do
    key
    |> to_charlist()
    |> Enum.map(&(&1 - ?a))
    |> Stream.cycle()
  end

  defp shift({char, _shift}, _direction) when char not in @letters, do: char

  defp shift({char, 0}, _direction), do: char

  defp shift({char, shift_by}, :encode) do
    shifted_char = char + shift_by

    cond do
      shifted_char > ?z -> shifted_char - @wrap_around_front
      :else -> shifted_char
    end
  end

  defp shift({char, shift_by}, :decode) do
    shifted_char = char - shift_by

    cond do
      shifted_char < ?a -> shifted_char + @wrap_around_front
      :else -> shifted_char
    end
  end
end
