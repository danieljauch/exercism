defmodule TwelveDays do
  @ordinals ~w(
    zeroth
    first
    second
    third
    fourth
    fifth
    sixth
    seventh
    eighth
    ninth
    tenth
    eleventh
    twelfth
  )
  @gifts [
    "nada",
    "a Partridge in a Pear Tree",
    "two Turtle Doves",
    "three French Hens",
    "four Calling Birds",
    "five Gold Rings",
    "six Geese-a-Laying",
    "seven Swans-a-Swimming",
    "eight Maids-a-Milking",
    "nine Ladies Dancing",
    "ten Lords-a-Leaping",
    "eleven Pipers Piping",
    "twelve Drummers Drumming"
  ]

  @doc """
  Given a `number`, return the song's verse for that specific day, including
  all gifts for previous days in the same line.
  """
  @spec verse(number :: integer) :: String.t()
  def verse(number) do
    verse_antecedent(number) <> verse_consequent(number)
  end

  @doc """
  Given a `starting_verse` and an `ending_verse`, return the verses for each
  included day, one per line.
  """
  @spec verses(starting_verse :: integer, ending_verse :: integer) :: String.t()
  def verses(starting_verse, ending_verse) do
    sing_verses(starting_verse, ending_verse)
  end

  def sing_verses(current_verse, ending_verse, song \\ [])
  def sing_verses(ending_verse, ending_verse, song) do
    song ++ [ending_verse |> verse]
    |> Enum.join("\n")
  end
  def sing_verses(current_verse, ending_verse, song) when current_verse < ending_verse do
    sing_verses(
      current_verse + 1,
      ending_verse,
      song ++ [current_verse |> verse]
    )
  end

  @doc """
  Sing all 12 verses, in order, one verse per line.
  """
  @spec sing() :: String.t()
  def sing do
    verses(1, 12)
  end

  defp verse_antecedent(number) do
    "On the #{Enum.at(@ordinals, number)} day of Christmas my true love gave to me: "
  end

  defp verse_consequent(number, sentence \\ "")
  defp verse_consequent(1, sentence) do
    if sentence == "" do
      Enum.at(@gifts, 1) <> "."
    else
      "#{sentence}and #{Enum.at(@gifts, 1)}."
    end
  end
  defp verse_consequent(number, sentence) when number > 0 do
    verse_consequent(
      number - 1,
      sentence <> Enum.at(@gifts, number) <> ", "
    )
  end
end
