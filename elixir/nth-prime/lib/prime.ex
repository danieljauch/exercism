defmodule Prime do
  @lowest_square 4
  @lowest_root 2

  @doc """
  Generates the nth prime.
  """
  @spec nth(non_neg_integer) :: non_neg_integer | no_return
  def nth(0), do: raise("Must be greater than 0")

  def nth(count) do
    @lowest_root
    |> Stream.iterate(&(&1 + 1))
    |> Stream.filter(&is_prime?/1)
    |> Enum.at(count - 1)
  end

  defp is_prime?(number) when number < @lowest_square, do: true

  defp is_prime?(number) do
    not has_prime_factor?(number)
  end

  defp has_prime_factor?(number) do
    number
    |> get_factor_range()
    |> Enum.any?(&is_factor?(number, &1))
  end

  defp get_factor_range(number) do
    end_of_range =
      number
      |> :math.sqrt()
      |> trunc()

    @lowest_root..end_of_range
  end

  defp is_factor?(multiple, factor) do
    rem(multiple, factor) == 0
  end
end
