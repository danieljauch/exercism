defmodule AllYourBase do
  @doc """
  Given a number in base a, represented as a sequence of digits, converts it to base b,
  or returns nil if either of the bases are less than 2
  """

  @spec convert(list, integer, integer) :: list
  def convert([], _from, _to), do: nil

  def convert(digits, from_base, to_base) do
    digits
    |> Enum.join()
    |> Integer.parse(from_base)
    |> elem(0)
    |> Integer.digits(to_base)
  end
end
