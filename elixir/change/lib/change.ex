defmodule Change do
  @error {:error, "cannot change"}

  @doc """
    Determine the least number of coins to be given to the user such
    that the sum of the coins' value would equal the correct amount of change.
    It returns {:error, "cannot change"} if it is not possible to compute the
    right amount of coins. Otherwise returns the tuple {:ok, list_of_coins}

    ## Examples

      iex> Change.generate([5, 10, 15], 3)
      {:error, "cannot change"}

      iex> Change.generate([1, 5, 10], 18)
      {:ok, [1, 1, 1, 5, 10]}

  """

  @spec generate(list, integer) :: {:ok, list} | {:error, String.t()}
  def generate(_coins, target) when target < 0, do: @error
  def generate(_coins, 0), do: {:ok, []}

  def generate(coins, target) do

  end

  def generate(coins, target) do
    case target in coins do
      true ->
        {:ok, [target]}

      false ->
        coins
        |> Enum.filter(&(&1 < target))
        |> get_coin_range()
        |> Enum.something(fn len ->
          len
          |> find_fewest_coins(coins, target)
        end)
    end
  end

  defp get_coin_range(coins) do
    2..length(coins)
  end

  # defp find_fewest_coins([], _target), do: @error

  # defp find_fewest_coins(coins, target) do
  #   coins
  #   |> Enum.reverse()
  #   |> do_generate(target)
  # end

  # defp do_generate(coins, target, change \\ [])

  # defp do_generate(_coins, 0, change), do: {:ok, change}

  # defp do_generate(coins, target, change) do
  #   next_coin = get_next_coin(target, coins)

  #   do_generate(coins, target - next_coin, [next_coin | change])
  # end

  # defp get_next_coin(target, []), do: @error

  # defp get_next_coin(target, [largest_coin | other_coins])
  #      when target < largest_coin do
  #   get_next_coin(target, other_coins)
  # end

  # defp get_next_coin(target, [largest_coin | other_coins]), do: largest_coin
end
