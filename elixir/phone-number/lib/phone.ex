defmodule Phone do
  @blank_out "0000000000"

  @doc """
  Remove formatting from a phone number.

  Returns "0000000000" if phone number is not valid
  (10 digits or "1" followed by 10 digits)

  ## Examples

  iex> Phone.number("212-555-0100")
  "2125550100"

  iex> Phone.number("+1 (212) 555-0100")
  "2125550100"

  iex> Phone.number("+1 (212) 055-0100")
  "0000000000"

  iex> Phone.number("(212) 555-0100")
  "2125550100"

  iex> Phone.number("867.5309")
  "0000000000"
  """
  @spec number(String.t()) :: String.t()
  def number(raw_input) do
    with false <- Regex.match?(~r/[a-z]/i, raw_input) do
      raw_input
      |> String.replace(~r/[^\d]/, "")
      |> validate()
    else
      _ ->
        @blank_out
    end
  end

  defp validate(phone_number) do
    case byte_size(phone_number) do
      11 -> validate_country_code(phone_number)
      10 -> validate_area_code(phone_number)
      _ -> @blank_out
    end
  end

  defp validate_country_code("1" <> <<phone_number::binary-10>>), do: phone_number
  defp validate_country_code(_invalid_number), do: @blank_out

  defp validate_area_code("0" <> <<phone_number::binary-9>>), do: @blank_out
  defp validate_area_code("1" <> <<phone_number::binary-9>>), do: @blank_out
  defp validate_area_code(phone_number), do: validate_exchange_code(phone_number)

  defp validate_exchange_code(<<_area::binary-3>> <> "0" <> <<_rest::binary-6>>), do: @blank_out
  defp validate_exchange_code(<<_area::binary-3>> <> "1" <> <<_rest::binary-6>>), do: @blank_out
  defp validate_exchange_code(<<phone_number::binary-10>>), do: phone_number

  @doc """
  Extract the area code from a phone number

  Returns the first three digits from a phone number,
  ignoring long distance indicator

  ## Examples

  iex> Phone.area_code("212-555-0100")
  "212"

  iex> Phone.area_code("+1 (212) 555-0100")
  "212"

  iex> Phone.area_code("+1 (012) 555-0100")
  "000"

  iex> Phone.area_code("867.5309")
  "000"
  """
  @spec area_code(String.t()) :: String.t()
  def area_code(raw) do
    <<area::binary-3, _rest::binary>> = number(raw)
    area
  end

  @doc """
  Pretty print a phone number

  Wraps the area code in parentheses and separates
  exchange and subscriber number with a dash.

  ## Examples

  iex> Phone.pretty("212-555-0100")
  "(212) 555-0100"

  iex> Phone.pretty("212-155-0100")
  "(000) 000-0000"

  iex> Phone.pretty("+1 (303) 555-1212")
  "(303) 555-1212"

  iex> Phone.pretty("867.5309")
  "(000) 000-0000"
  """
  @spec pretty(String.t()) :: String.t()
  def pretty(raw) do
    <<area::binary-3, exchange::binary-3, subscriber::binary-4>> = number(raw)

    "(#{area}) #{exchange}-#{subscriber}"
  end
end
