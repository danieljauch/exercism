defmodule Atbash do
  @alphabet ?a..?z
  @allowed_non_alphas ?0..?9

  @doc """
  Encode a given plaintext to the corresponding ciphertext

  ## Examples

  iex> Atbash.encode("completely insecure")
  "xlnko vgvob rmhvx fiv"
  """
  @spec encode(String.t()) :: String.t()
  def encode(input) do
    use_cipher(input, :encode)
  end

  @spec decode(String.t()) :: String.t()
  def decode(input) do
    use_cipher(input, :decode)
  end

  defp use_cipher(input, direction) do
    input
    |> process_input()
    |> format_output(direction)
  end

  defp process_input(input) do
    input
    |> String.downcase()
    |> to_charlist()
    |> Enum.map(fn
      char when char in @alphabet -> ?a + (?z - char)
      char when char in @allowed_non_alphas -> char
      _ -> ''
    end)
  end

  defp format_output(encoded_input, :encode) do
    encoded_input
    |> to_string()
    |> String.codepoints()
    |> Enum.chunk_every(5)
    |> Enum.map(&Enum.join/1)
    |> Enum.join(" ")
  end

  defp format_output(encoded_input, :decode), do: to_string(encoded_input)
end
