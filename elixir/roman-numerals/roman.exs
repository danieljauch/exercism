defmodule Roman do
  @thousands %{
    one: "M"
  }
  @hundreds %{
    one: "C",
    five: "D",
    ten: "M"
  }
  @tens %{
    one: "X",
    five: "L",
    ten: "C"
  }
  @ones %{
    one: "I",
    five: "V",
    ten: "X"
  }

  @doc """
  Convert the number to a roman number.
  """
  @spec numerals(pos_integer) :: String.t()
  def numerals(arabic_number) do
    digits = Integer.digits(arabic_number)

    [:thousands, :hundreds, :tens, :ones]
    |> Enum.drop(4 - length(digits))
    |> Enum.zip(digits)
    |> Enum.map(fn {column, digit} ->
      arabic_to_roman(digit, column)
    end)
    |> List.to_string()
  end

  defp arabic_to_roman(digit, column) do
    set =
      case column do
        :thousands -> @thousands
        :hundreds -> @hundreds
        :tens -> @tens
        _ -> @ones
      end

    digit
    |> atoms_from_arabic_number()
    |> Enum.map(&Map.get(set, &1))
  end

  defp atoms_from_arabic_number(n) do
    case n do
      9 -> [:one, :ten]
      8 -> [:five, :one, :one, :one]
      7 -> [:five, :one, :one]
      6 -> [:five, :one]
      5 -> [:five]
      4 -> [:one, :five]
      3 -> [:one, :one, :one]
      2 -> [:one, :one]
      1 -> [:one]
      _ -> []
    end
  end
end
