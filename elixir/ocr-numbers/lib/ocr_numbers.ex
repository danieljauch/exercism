defmodule OcrNumbers do
  @doc """
  Given a 3 x 4 grid of pipes, underscores, and spaces, determine which number is represented, or
  whether it is garbled.
  """
  @spec convert([String.t()]) :: String.t()
  def convert(input) do
    with :ok <- validate_lines(input),
         chunked_digits <- chunk_input(input) do
      {:ok, compare_input(chunked_digits)}
    end
  end

  defp validate_lines(ocr_digit) do
    valid_digits =
      ocr_digit
      |> length()
      |> rem(4)
      |> Kernel.==(0)

    if valid_digits do
      validate_columns(ocr_digit)
    else
      {:error, 'invalid line count'}
    end
  end

  defp validate_columns([head | tail]) do
    valid_digits =
      head
      |> String.length()
      |> rem(3)
      |> Kernel.==(0)

    if valid_digits do
      validate_columns(tail)
    else
      {:error, 'invalid column count'}
    end
  end

  defp validate_columns([]), do: :ok

  defp chunk_input(input) do
    dimensions = %{
      columns: count_columns(input),
      rows: count_rows(input)
    }
    |> IO.inspect(label: "dimensions")

    input
    |> split_into_grid(dimensions)
  end

  defp compare_input([" _ ", "| |", "|_|", "   "]), do: "0"
  defp compare_input(["   ", "  |", "  |", "   "]), do: "1"
  defp compare_input([" _ ", " _|", "|_ ", "   "]), do: "2"
  defp compare_input([" _ ", " _|", " _|", "   "]), do: "3"
  defp compare_input(["   ", "|_|", "  |", "   "]), do: "4"
  defp compare_input([" _ ", "|_ ", " _|", "   "]), do: "5"
  defp compare_input([" _ ", "|_ ", "|_|", "   "]), do: "6"
  defp compare_input([" _ ", "  |", "  |", "   "]), do: "7"
  defp compare_input([" _ ", "|_|", "|_|", "   "]), do: "8"
  defp compare_input([" _ ", "|_|", " _|", "   "]), do: "9"
  defp compare_input(_), do: "?"

  defp count_columns(input) do
    input
    |> Enum.at(0)
    |> String.length()
    |> div(3)
  end

  defp count_rows(input) do
    input
    |> length()
    |> div(4)
  end

  defp split_into_grid(input, %{columns: columns, rows: rows}) do
    input
    |> split_columns(columns)
    |> split_rows(rows)
  end

  defp split_columns(input, columns) do
    input
    |> IO.inspect(label: "columns")
  end

  defp split_rows(input, rows) do
    input
    |> IO.inspect(label: "rows")
  end
end
