defmodule Queens do
  @type t :: %Queens{black: {integer, integer}, white: {integer, integer}}
  defstruct [:white, :black]

  @doc """
  Creates a new set of Queens
  """
  @spec new(Keyword.t()) :: Queens.t()
  def new(opts \\ [])

  def new(queens) do
    white = Keyword.get(queens, :white)
    black = Keyword.get(queens, :black)

    with :ok <- valid_pos(white),
         :ok <- valid_pos(black),
         false <- on_same_space?(white, black) do
      %Queens{white: white, black: black}
    else
      error ->
        raise error
    end
  end

  def new(_), do: raise(ArgumentError)

  defp valid_pos({x, y}) when x in 0..7 and y in 0..7, do: :ok
  defp valid_pos(nil), do: :ok
  defp valid_pos(_pos), do: ArgumentError

  defp on_same_space?(same_pos, same_pos), do: ArgumentError
  defp on_same_space?(_pos_a, _pos_b), do: false

  @doc """
  Gives a string representation of the board with
  white and black queen locations shown
  """
  @spec to_string(Queens.t()) :: String.t()
  def to_string(%Queens{white: white, black: black}) do
    white_pos = calc_pos(white)
    black_pos = calc_pos(black)

    0..63
    |> Enum.map(fn value ->
      case value == white_pos do
        true ->
          "W"

        _ ->
          case value == black_pos do
            true ->
              "B"

            _ ->
              "_"
          end
      end
    end)
    |> Enum.with_index()
    |> Enum.reduce("", fn {square, index}, acc ->
      case rem(index + 1, 8) do
        0 ->
          acc <> square <> "\n"

        _ ->
          acc <> square <> " "
      end
    end)
    |> String.trim()
  end

  defp calc_pos(nil), do: -1
  defp calc_pos({x, y}), do: x * 8 + y

  @doc """
  Checks if the queens can attack each other
  """
  @spec can_attack?(Queens.t()) :: boolean
  def can_attack?(%Queens{white: nil}), do: false
  def can_attack?(%Queens{black: nil}), do: false

  def can_attack?(%Queens{white: {x, _white_y}, black: {x, _black_y}}), do: true
  def can_attack?(%Queens{white: {_white_x, y}, black: {_black_x, y}}), do: true

  def can_attack?(%Queens{
        white: {white_x, white_y},
        black: {black_x, black_y}
      }) do
    abs(white_x - black_x) == abs(white_y - black_y)
  end

  def can_attack(_), do: false
end
