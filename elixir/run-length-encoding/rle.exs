defmodule RunLengthEncoder do
  @doc """
  Generates a string where consecutive elements are represented as a data value and count.
  "AABBBCCCC" => "2A3B4C"
  For this example, assume all input are strings, that are all uppercase letters.
  It should also be able to reconstruct the data into its original form.
  "2A3B4C" => "AABBBCCCC"
  """
  @spec encode(String.t()) :: String.t()
  def encode(""), do: ""

  def encode(string) do
    string
    |> String.codepoints()
    |> Enum.reverse()
    |> build_counts()
    |> Enum.reduce("", &do_encode/2)
  end

  defp build_counts(codepoints, acc \\ [])

  defp build_counts([], acc), do: acc

  defp build_counts([matching_point | tail], [{matching_point, point_count} | acc_tail]) do
    build_counts(tail, [{matching_point, point_count + 1} | acc_tail])
  end

  defp build_counts([next_point | tail], acc) do
    build_counts(tail, [{next_point, 1} | acc])
  end

  defp do_encode({codepoint, 1}, acc), do: acc <> codepoint
  defp do_encode({codepoint, count}, acc), do: acc <> Integer.to_string(count) <> codepoint

  @spec decode(String.t()) :: String.t()
  def decode(""), do: ""

  def decode(string) do
    ~r/\d+[^\d]|./
    |> Regex.scan(string)
    |> Enum.reduce([], &[to_count_tuple(&1) | &2])
    |> build_string_from_counts()
  end

  defp to_count_tuple([encoded_charcter]) do
    string_length = String.length(encoded_charcter)

    case string_length do
      1 ->
        {encoded_charcter, 1}

      _ ->
        {string_count, codepoint} = String.split_at(encoded_charcter, string_length - 1)
        {codepoint, String.to_integer(string_count)}
    end
  end

  defp build_string_from_counts(tuples, acc \\ [])

  defp build_string_from_counts([], acc), do: Enum.join(acc)

  defp build_string_from_counts([{next_point, 1} | tail], acc) do
    build_string_from_counts(tail, [next_point | acc])
  end

  defp build_string_from_counts([{next_point, count} | tail], acc) do
    build_string_from_counts(tail, [next_point_string(next_point, count) | acc])
  end

  defp next_point_string(codepoint, count) do
    [codepoint]
    |> Stream.cycle()
    |> Enum.take(count)
    |> Enum.join()
  end
end
