defmodule BankAccount do
  use GenServer

  @moduledoc """
  A bank account that supports access from multiple processes.
  """

  @typedoc """
  An account handle.
  """
  @opaque account :: pid

  @doc """
  Open the bank. Makes the account available.
  """
  @spec open_bank() :: account
  def open_bank() do
    {:ok, balance} = GenServer.start_link(__MODULE__, [])
    balance
  end

  @doc """
  Close the bank. Makes the account unavailable.
  """
  @spec close_bank(account) :: none
  def close_bank(account), do: GenServer.cast(account, :close)

  @doc """
  Get the account's balance.
  """
  @spec balance(account) :: integer
  def balance(balance), do: GenServer.call(balance, :balance)

  @doc """
  Update the account's balance by adding the given amount which may be negative.
  """
  @spec update(account, integer) :: any
  def update(account, balance), do: GenServer.call(account, balance)

  # INTERNAL BEHAVIOR

  def init([]), do: {:ok, 0}

  def handle_call(_, _from, :closed), do: {:reply, {:error, :account_closed}, :closed}
  def handle_call(:balance, _from, balance), do: {:reply, balance, balance}

  def handle_call(amount, _from, balance) do
    new_balance = balance + amount
    {:reply, new_balance, new_balance}
  end

  def handle_cast(:close, _from), do: {:noreply, :closed}
end
