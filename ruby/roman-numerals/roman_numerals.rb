module RomanNumeral
  NUMERALS = {
    ones: {
      one: 'I',
      five: 'V',
      ten: 'X'
    },
    tens: {
      one: 'X',
      five: 'L',
      ten: 'C'
    },
    hundreds: {
      one: 'C',
      five: 'D',
      ten: 'M'
    },
    thousands: {
      one: 'M'
    }
  }.freeze

  def to_roman
    number = self

    number
      .digits
      .zip(%i[ones tens hundreds thousands])
      .reverse
      .flat_map { |digit, column| map_to_roman_numeral(digit, column) }
      .join('')
  end

  private

  def map_to_roman_numeral(digit, column)
    roman_letters = NUMERALS[column]
    symbols_from_arabic(digit).map { |sym| roman_letters[sym] }
  end

  def symbols_from_arabic(arabic)
    case arabic
    when 9
      %i[one ten]
    when 8
      %i[five one one one]
    when 7
      %i[five one one]
    when 6
      %i[five one]
    when 5
      [:five]
    when 4
      %i[one five]
    when 3
      %i[one one one]
    when 2
      %i[one one]
    when 1
      [:one]
    else
      []
    end
  end
end

Integer.include(RomanNumeral)
