class Hamming
  def self.compute(left, right)
    raise ArgumentError if left.length != right.length

    (0..left.length).count { |i| left[i] != right[i] }
  end
end
