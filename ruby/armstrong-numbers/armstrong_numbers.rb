class ArmstrongNumbers
  def self.include?(number)
    return true if number < 10

    sum_digits(number) == number
  end

  def self.sum_digits(number)
    digits = number.digits
    power = digits.length

    digits.sum { |digit| digit.pow(power) }
  end

  private_class_method :sum_digits
end
