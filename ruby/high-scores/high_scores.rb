class HighScores
  attr_reader :scores, :latest, :personal_best

  def initialize(scores)
    @scores = scores
    @latest = scores.last
    @personal_best = scores.max
  end

  def personal_top_three
    return @scores if @scores.length == 1

    @scores.sort do |left, right|
      right <=> left
    end.take(3)
  end

  def latest_is_personal_best?
    @latest == @personal_best
  end
end
