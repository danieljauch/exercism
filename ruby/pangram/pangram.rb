require 'set'

class Pangram
  ALPHABET_SIZE = 26

  def self.pangram?(sentence)
    normalized_sentence = sentence.downcase.scan(/[a-z]/)
    return false if normalized_sentence.length < ALPHABET_SIZE

    Set.new(normalized_sentence).size == ALPHABET_SIZE
  end
end
