class Bob
  YELLING_QUESTION = "Calm down, I know what I'm doing!".freeze
  QUESTION = 'Sure.'.freeze
  YELLING = 'Whoa, chill out!'.freeze
  SILENT = 'Fine. Be that way!'.freeze
  DEFAULT = 'Whatever.'.freeze

  def self.hey(input)
    remark = normalize_input(input)

    return YELLING_QUESTION if yelling?(remark) && question?(remark)
    return QUESTION if question?(remark)
    return YELLING if yelling?(remark)
    return SILENT if silent?(remark)

    DEFAULT
  end

  def self.normalize_input(input)
    input.gsub(/[^\p{L}\d?!]/, '')
  end

  def self.question?(remark)
    remark.end_with?('?')
  end

  def self.yelling?(remark)
    alphas_only = remark.gsub(/[^\p{L}]/, '')

    !silent?(alphas_only) && (alphas_only.upcase == alphas_only)
  end

  def self.silent?(remark)
    remark.empty?
  end

  private_class_method :normalize_input, :question?, :yelling?, :silent?
end
