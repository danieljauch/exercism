require 'set'

class Isogram
  def self.isogram?(text)
    return true if text.empty?

    letters = text.downcase.scan(/\w/)
    letters.length == Set.new(letters).size
  end
end
