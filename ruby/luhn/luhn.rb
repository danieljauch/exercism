class Luhn
  def self.valid?(number_text)
    return false if number_text[/[^(\d|\s)]/]

    normalized_text = number_text.scan(/\d/)
    return false if normalized_text.length < 2

    (checksum(normalized_text) % 10).zero?
  end

  def checksum(number_text)
    number_text
      .reverse
      .map
      .with_index do |digit, index|
        next_digit = digit.to_i
        index.odd? ? new_value(next_digit) : next_digit
      end
      .sum
  end

  def new_value(number)
    product = number * 2
    product > 9 ? product - 9 : product
  end

  private_class_method :checksum, :new_value
end
