class Prime
  LOWEST_SQUARE = 4
  LOWEST_ROOT = 2

  def self.nth(count)
    raise ArgumentError if count < 1

    (LOWEST_ROOT..)
      .lazy
      .filter { |n| prime?(n) }
      .take(count)
      .force
      .last
  end

  def self.prime?(number)
    return true if number < LOWEST_SQUARE

    !has_prime_factor?(number)
  end

  def self.has_prime_factor?(number)
    (LOWEST_ROOT..Integer.sqrt(number).floor)
      .any? { |maybe_factor| factor?(number, maybe_factor) }
  end

  def self.factor?(multiple, number)
    (multiple % number).zero?
  end

  private_class_method :prime?, :has_prime_factor?, :factor?
end
