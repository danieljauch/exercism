module Accumulate
  def accumulate
    return self unless block_given?

    each_with_object([]) { |elem, acc| acc << yield(elem) }
  end
end

Array.include(Accumulate)
