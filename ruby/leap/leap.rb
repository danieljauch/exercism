class Year
  def self.leap?(year)
    return true if multiple?(year, 400)
    return false if multiple?(year, 100)
    return true if multiple?(year, 4)

    false
  end

  def self.multiple?(year, factor)
    (year % factor).zero?
  end

  private_class_method :multiple?
end
