module Strain
  def keep(&block)
    return self if empty? || !block_given?

    filter_for(true, &block)
  end

  def discard(&block)
    return self if empty? || !block_given?

    filter_for(false, &block)
  end

  private

  def filter_for(bool, &condition)
    each_with_object([]) do |elem, acc|
      acc << elem if condition.call(elem) == bool
    end
  end
end

Array.include(Strain)
