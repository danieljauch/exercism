class Complement
  NUCLEOTIDES = {
    'A' => 'U',
    'C' => 'G',
    'G' => 'C',
    'T' => 'A'
  }.freeze

  def self.of_dna(strand)
    strand.chars.map { |dna| NUCLEOTIDES[dna] }.join('')
  end
end
