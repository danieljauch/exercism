class BeerSong
  def self.recite(from, count)
    from
      .downto(from - count)
      .map { |i| sing_line(i) }
      .join('\n')
  end

  def self.sing_line(number)
    case number
    when 0
      <<~TEXT
        No more bottles of beer on the wall, no more bottles of beer.
        Go to the store and buy some more, 99 bottles of beer on the wall.
      TEXT
    when 1
      <<~TEXT
        1 bottle of beer on the wall, 1 bottle of beer.
        Take it down and pass it around, no more bottles of beer on the wall.
      TEXT
    else
      next_number = number - 1

      <<~TEXT
        #{number} #{pluralize('bottle', number)} of beer on the wall, #{number} #{pluralize('bottle', number)} of beer.
        Take one down and pass it around, #{next_number} #{pluralize('bottle', next_number)} of beer on the wall.
      TEXT
    end
  end

  def self.pluralize(word, count)
    return word if count == 1

    word << 's'
  end

  private_class_method :sing_line, :pluralize
end
