class Raindrops
  @sounds = { 3 => 'Pling', 5 => 'Plang', 7 => 'Plong' }

  def self.convert(number)
    output = map_sounds(number).join('')

    return output unless output == ''

    number.to_s
  end

  def map_sounds(number)
    @sounds.map do |prime, sound|
      if factor?(number, prime)
        sound
      else
        ''
      end
    end
  end

  def factor?(number, factor)
    (number % factor).zero?
  end

  private_class_method :map_sounds, :factor?
end
