(ns bob (:require [clojure.string :as string]))

(defn- question? [call] (= \? (last call)))
(defn- shouting? [call]
  (and
   (= call (string/upper-case call))
   (re-seq #"(?i) \p{L}" call)))

(defn response-for [call]
  (cond
    (string/blank? call) "Fine. Be that way!"
    (and (question? call) (shouting? call)) "Calm down, I know what I'm doing!"
    (question? call) "Sure."
    (shouting? call) "Whoa, chill out!"
    :else "Whatever."))
