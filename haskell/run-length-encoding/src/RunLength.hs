module RunLength (decode, encode) where

import Data.Char (isDigit)

decode :: String -> String
decode [] = []
decode text = (take n $ repeat h) ++ (decode t)
  where
    (nStr, rest) = span isDigit text
    n = readCount nStr
    (h:t) = rest

readCount :: String -> Int
readCount "" = 1
readCount count = read count

encode :: String -> String
encode [] = []
encode (c:cs) = charLength ++ [c] ++ encode rest
  where
    (chars, rest) = span (==c) cs
    charLength = showCount $ length chars

showCount :: Int -> String
showCount 0 = ""
showCount n = show $ n + 1
