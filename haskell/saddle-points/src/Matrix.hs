module Matrix (saddlePoints) where

import Data.Array (Array, bounds, (!))

saddlePoints :: Array (Int, Int) Int -> [(Int, Int)]
saddlePoints matrix =
  [ (x', y')
    | x' <- [0 .. x],
      y' <- [0 .. y],
      matrix ! (x', y') == maxRow y',
      matrix ! (x', y') == minCol x'
  ]
  where
    (_, (cols, rows)) = bounds matrix
    maxRow row = maximum [matrix ! (row, b) | b <- [0 .. rows]]
    minCol col = minimum [matrix ! (a, col) | a <- [0 .. cols]]
