module IsbnVerifier (isbn, isValid) where

import Data.Char (digitToInt)
import Data.List (elemIndex)

isbn :: String -> Bool
isbn raw
  | l /= 10 = False
  | wrongXPos = False
  | invalidCheckDigit = False
  | otherwise = isValid normal
  where
    normal = filter (`elem` validChars) raw
    l = length normal
    invalidCheckDigit = not $ (`elem` validChars) . last $ raw
    wrongXPos = xInBadPos . (elemIndex 'X') $ raw

xInBadPos :: Maybe Int -> Bool
xInBadPos Nothing = False
xInBadPos (Just 12) = False
xInBadPos _ = True

validChars :: [Char]
validChars = 'X' : ['0' .. '9']

isValid :: [Char] -> Bool
isValid = (== 0) . (`mod` 11) . sum . zipWith (*) [10,9..1] . map convert

convert :: Char -> Int
convert 'X' = 10
convert n = digitToInt n
