module CollatzConjecture (collatz) where

collatz :: Integer -> Maybe Integer
collatz n
  | n < 1 = Nothing
  | n == 1 = Just 0
  | otherwise = fmap (+ 1) . collatz $ nextNumber n

nextNumber :: Integer -> Integer
nextNumber n
  | even n = n `div` 2
  | otherwise = n * 3 + 1
