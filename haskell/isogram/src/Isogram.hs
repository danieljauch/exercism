module Isogram (isIsogram) where

import Data.Char (isAlpha, toLower)

isIsogram :: String -> Bool
isIsogram phrase = isIsogram' (normalizedPhrase phrase) ""

normalizedPhrase :: [Char] -> [Char]
normalizedPhrase phrase = normalizedPhrase' phrase ""

normalizedPhrase' :: [Char] -> [Char] -> [Char]
normalizedPhrase' [] correctedPhrase = correctedPhrase
normalizedPhrase' (char : phrase) acc
  | isAlpha char = normalizedPhrase' phrase $ toLower char : acc
  | otherwise = normalizedPhrase' phrase acc

isIsogram' :: String -> String -> Bool
isIsogram' [] _ = True
isIsogram' (char : phrase) acc
  | char `elem` acc = False
  | otherwise = isIsogram' phrase (char : acc)
