module School (School, add, School.empty, grade, sorted) where

import Data.List (sort)
import qualified Data.Map as Map (Map, empty, insert, lookup, toList)

type School = Map.Map Int [String]

add :: Int -> String -> School -> School
add gradeNum student school =
  Map.insert gradeNum newGrade school
  where
    gradeStudents = grade gradeNum school
    newGrade = sort $ student : gradeStudents

empty :: School
empty = Map.empty

grade :: Int -> School -> [String]
grade gradeNum school = gradeOrDefault $ Map.lookup gradeNum school

gradeOrDefault :: Maybe [String] -> [String]
gradeOrDefault Nothing = []
gradeOrDefault (Just gradeStudents) = gradeStudents

sorted :: School -> [(Int, [String])]
sorted school = Map.toList school
