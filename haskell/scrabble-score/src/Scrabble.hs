module Scrabble (scoreLetter, scoreWord) where

import Data.Char (isAlpha, toLower)

scoreLetter :: Char -> Int
scoreLetter letter
  | isAlpha letter = getScore $ toLower letter
  | otherwise = 0

scoreWord :: String -> Int
scoreWord word = sum [scoreLetter c | c <- word]

getScore :: Char -> Int
getScore c
  | c `elem` "aeioulnrst" = 1
  | c `elem` "dg" = 2
  | c `elem` "bcmp" = 3
  | c `elem` "fhvwy" = 4
  | c == 'k' = 5
  | c `elem` "jx" = 8
  | c `elem` "qz" = 10
  | otherwise = 0
