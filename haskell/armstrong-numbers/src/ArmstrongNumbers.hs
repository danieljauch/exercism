module ArmstrongNumbers (armstrong) where

import Data.Char (digitToInt)

armstrong :: Int -> Bool
armstrong 0 = True
armstrong n
  | count == 1 = True
  | count == 2 = False
  | otherwise = n == (reduceDigits digits count)
  where
    digits = readDigits n
    count = length digits

readDigits :: Int -> [Int]
readDigits = map digitToInt . show

reduceDigits :: [Int] -> Int -> Int
reduceDigits digits power = sum [x ^ power | x <- digits]
