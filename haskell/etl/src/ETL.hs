module ETL (transform) where

import Data.Char (toLower)
import Data.Map (Map, fromList, toList)

transform :: (Num a) => Map a String -> Map Char a
transform legacyData = fromList . concatMap (makeCharTuples) $ toList legacyData

makeCharTuples :: (Num a) => (a, [Char]) -> [(Char, a)]
makeCharTuples (count, chars) = [(toLower char, count) | char <- chars]
