module Triplet (tripletsWithSum) where

tripletsWithSum :: Int -> [(Int, Int, Int)]
tripletsWithSum upTo =
  [ (a, b, c)
    | b <- [1 .. upTo],
      a <- [1 .. b],
      let c = upTo - (a + b),
      a ^ 2 + b ^ 2 == c ^ 2
  ]
