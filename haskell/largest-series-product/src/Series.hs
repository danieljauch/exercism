module Series (Error (..), largestProduct) where

import Data.Char (digitToInt, isDigit)
import Data.List (find, tails)
import Data.Maybe (fromJust, isJust)

data Error = InvalidSpan | InvalidDigit Char deriving (Show, Eq)

largestProduct :: Int -> String -> Either Error Int
largestProduct size digits
  | isJust badDigit = Left . InvalidDigit $ fromJust badDigit
  | size < 0 || size > length digits = Left InvalidSpan
  | size == 0 = Right 1
  | size == length digits = Right $ product digitsAsInts
  | otherwise = Right . maximum . map (product . take size) $ tails digitsAsInts
  where
    digitsAsInts = map digitToInt digits
    badDigit = invalidDigit digits

invalidDigit :: [Char] -> Maybe Char
invalidDigit = find (not . isDigit)
