module ResistorColors (Color (..), Resistor (..), label, ohms) where

data Color
  = Black
  | Brown
  | Red
  | Orange
  | Yellow
  | Green
  | Blue
  | Violet
  | Grey
  | White
  deriving (Show, Enum, Bounded)

newtype Resistor = Resistor {bands :: (Color, Color, Color)}
  deriving (Show)

label :: Resistor -> String
label resistor = abbreviate o ++ " " ++ prefix o ++ "ohms"
  where
    o = ohms resistor

abbreviate :: (Num a, Ord a) => a -> String
abbreviate n
  | n < 10 ^ 3 = show n
  | n < 10 ^ 6 = show (n / 10 ^ 3) ++ "e3"
  | n < 10 ^ 9 = show (n / 10 ^ 6) ++ "e6"
  | otherwise = show (n / 10 ^ 9) ++ "e9"

prefix :: (Num a, Ord a) => a -> String
prefix n
  | n < 10 ^ 3 = ""
  | n < 10 ^ 6 = "kilo"
  | n < 10 ^ 9 = "mega"
  | otherwise = "giga"

ohms :: Resistor -> Int
ohms (Resistor (a, b, c)) = base (a, b) * 10 ^ (fromEnum c)

base :: (Color, Color) -> Int
base (a, b) = fromEnum a * 10 + fromEnum b
