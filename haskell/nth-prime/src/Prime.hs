module Prime (nth) where

import Data.List as List (foldl)
import Data.Map as Map (Map, delete, empty, insert, insertWith, lookup)

nth :: Int -> Maybe Integer
nth n
  | n < 1 = Nothing
  | otherwise = Just . last $ take n primes

primes :: [Integer]
primes = sieve [2 ..]

sieve :: (Ord k, Num k) => [k] -> [k]
sieve xs = sieve' xs Map.empty

sieve' :: (Ord k, Num k) => [k] -> Map k [k] -> [k]
sieve' [] _ = []
sieve' (x : xs) table = case Map.lookup x table of
  Nothing -> x : sieve' xs (Map.insert (x * x) [x] table)
  Just facts -> sieve' xs $ List.foldl reinsert (Map.delete x table) facts
  where
    reinsert table' prime = Map.insertWith (++) (x + prime) [prime] table'
