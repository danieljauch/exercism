module Anagram (anagramsFor) where

import Data.Char (toLower)
import Data.List (delete)

anagramsFor :: String -> [String] -> [String]
anagramsFor word = filter (isAnagramFor lowerWord . map toLower)
  where
    lowerWord = map (toLower) word

isAnagramFor :: String -> String -> Bool
isAnagramFor l r
  | l == r = False
  | otherwise = isAnagramFor' l r

isAnagramFor' :: String -> String -> Bool
isAnagramFor' [] (_h : _) = False
isAnagramFor' (_h : _) [] = False
isAnagramFor' [] [] = True
isAnagramFor' (h : t) r
  | h `elem` r = isAnagramFor' t $ delete h r
  | otherwise = False
