module Triangle (TriangleType(..), triangleType) where

import Data.List (group, sort)

data TriangleType = Equilateral
                  | Isosceles
                  | Scalene
                  | Illegal
                  deriving (Eq, Show)

triangleType :: (Num a, Ord a) => a -> a -> a -> TriangleType
triangleType a b c
  | any (==0) raw || sorted!!0 + sorted!!1 < sorted!!2 = Illegal
  | groups == 1 = Equilateral
  | groups == 2 = Isosceles
  | otherwise = Scalene
  where
    raw = [a,b,c]
    sorted = sort raw
    groups = length $ group sorted
