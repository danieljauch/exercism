module Diamond (diamond) where

import Data.Char (digitToInt, intToDigit, toUpper)
import Data.List (elemIndex)

diamond :: Char -> Maybe [String]
diamond char
  | char `elem` charRange = Just $ buildDiamond index char
  | otherwise = Nothing
  where
    index = charIndex char

charRange :: [Char]
charRange = ['A' .. 'Z']

charIndex :: Char -> Int
charIndex char = definiteIndex $ char `elemIndex` charRange

definiteIndex :: Maybe Int -> Int
definiteIndex Nothing = 0
definiteIndex (Just index) = index

buildDiamond :: Int -> Char -> [String]
buildDiamond rowSize char = buildDiamond' rowSize (buildFullCharList char) []

buildDiamond' :: Int -> [Char] -> [String] -> [String]
buildDiamond' _ [] result = result
buildDiamond' rowSize (char : rest) acc = buildDiamond' rowSize rest $ buildRow char rowSize : acc

buildRow :: Char -> Int -> String
buildRow char rowSize = [char]

buildFullCharList :: Char -> [Char]
buildFullCharList char = ['A' .. char] ++ [charLess char 1, charLess char 2 .. 'A']

charLess :: Char -> Int -> Char
charLess char less = toUpper . intToDigit $ digitToInt char - less
