module Allergies (Allergen (..), allergies, isAllergicTo) where

import Data.Bits (testBit)

data Allergen
  = Eggs
  | Peanuts
  | Shellfish
  | Strawberries
  | Tomatoes
  | Chocolate
  | Pollen
  | Cats
  deriving (Bounded, Enum, Eq, Show)

allergies :: Int -> [Allergen]
allergies = flip filter [minBound .. maxBound] . flip isAllergicTo

isAllergicTo :: Allergen -> Int -> Bool
isAllergicTo = flip testBit . fromEnum
