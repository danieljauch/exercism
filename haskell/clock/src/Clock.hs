module Clock (addDelta, fromHourMin, toString) where

data Clock = Clock Int Int deriving (Eq)

fromHourMin :: Int -> Int -> Clock
fromHourMin h m = Clock hours minutes
  where
    (hourRollover, minutes) = m `divMod` 60
    hours = (`mod` 24) . (+ hourRollover) $ h

toString :: Clock -> String
toString (Clock h m) = showTime h ++ ":" ++ showTime m

showTime :: Int -> String
showTime n
  | length digits > 1 = digits
  | otherwise = '0' : digits
  where
    digits = show n

addDelta :: Int -> Int -> Clock -> Clock
addDelta h m (Clock h' m') = fromHourMin (h + h') (m + m')
