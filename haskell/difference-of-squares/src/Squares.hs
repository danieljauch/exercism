module Squares (difference, squareOfSum, sumOfSquares) where

difference :: Integral a => a -> a
difference x = squareOfSum x - sumOfSquares x

rangeTo :: Integral a => a -> [a]
rangeTo x = [1 .. x]

squareOfSum :: Integral a => a -> a
squareOfSum = (^ 2) . sum . rangeTo

sumOfSquares :: Integral a => a -> a
sumOfSquares = sum . map (^ 2) . rangeTo
