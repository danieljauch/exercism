module Raindrops (convert) where

convert :: Int -> String
convert n
  | n < 3 || mapped == "" = show n
  | otherwise = mapped
  where
    mapped = soundMap n

sounds :: [(Int, String)]
sounds = [(3, "Pling"), (5, "Plang"), (7, "Plong")]

soundMap :: Int -> String
soundMap n = concat [sound | (x, sound) <- sounds, n `mod` x == 0]
