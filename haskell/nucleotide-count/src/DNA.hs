module DNA (nucleotideCounts, Nucleotide (..), getNucs) where

import Data.List (reverse, zip)
import Data.Map as Map (Map, fromList, insert, lookup)

data Nucleotide
  = A
  | C
  | G
  | T
  deriving (Eq, Ord, Show)

defaultMap :: Map.Map Nucleotide Int
defaultMap = Map.fromList . zip [A, C, G, T] $ cycle [0]

nucleotideCounts :: String -> Either String (Map.Map Nucleotide Int)
nucleotideCounts = outputCounts . getNucs

outputCounts ::
  Either String [Nucleotide] ->
  Either String (Map.Map Nucleotide Int)
outputCounts (Left errorMessage) = Left errorMessage
outputCounts (Right nucList) = Right $ reduceCounts nucList defaultMap

reduceCounts ::
  [Nucleotide] -> Map.Map Nucleotide Int -> Map.Map Nucleotide Int
reduceCounts [] nucMap = nucMap
reduceCounts (h : t) nucMap = reduceCounts t $ Map.insert h newCount nucMap
  where
    currentCount = Map.lookup h nucMap
    defaultedCount = definiteCount currentCount
    newCount = defaultedCount + 1

definiteCount :: Maybe Int -> Int
definiteCount Nothing = 0
definiteCount (Just n) = n

validChars :: [Char]
validChars = "ACGT"

getNucs :: String -> Either String [Nucleotide]
getNucs nucList = getNucs' nucList []

getNucs' :: String -> [Nucleotide] -> Either String [Nucleotide]
getNucs' [] acc = Right $ reverse acc
getNucs' (h : t) acc
  | h `elem` validChars = getNucs' t (nextNuc : acc)
  | otherwise = Left "Invalid sequence"
  where
    nextNuc = nucFromChar h

nucFromChar :: Char -> Nucleotide
nucFromChar 'A' = A
nucFromChar 'C' = C
nucFromChar 'G' = G
nucFromChar 'T' = T
nucFromChar _ = A
