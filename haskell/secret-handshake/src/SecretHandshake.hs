module SecretHandshake (handshake) where

import Data.Bits (clearBit, testBit)

handshake :: Int -> [String]
handshake n
  | testBit n 4 = reverse $ handshake (clearBit n 4)
  | testBit n 3 = "jump" : handshake (clearBit n 3)
  | testBit n 2 = "close your eyes" : handshake (clearBit n 2)
  | testBit n 1 = "double blink" : handshake (clearBit n 1)
  | n == 1 = "wink" : handshake (clearBit n 0)
  | otherwise = []

handshake' :: Int -> [String] -> [String]
handshake' n acc
  | testBit n 4 = reverse $ handshake' (clearBit n 4) acc
  | testBit n 3 = handshake' (clearBit n 3) ("jump" : acc)
  | testBit n 2 = handshake' (clearBit n 2) ("close your eyes" : acc)
  | testBit n 1 = handshake' (clearBit n 1) ("double blink" : acc)
  | n == 1 = handshake' (clearBit n 0) ("wink" : acc)
  | otherwise = acc
