module Triangle (rows) where

rows :: Int -> [[Integer]]
rows n = take n $ iterate (nextRow . (0 :)) [1]

nextRow :: (Num a) => [a] -> [a]
nextRow prevRow
  | length prevRow == 1 = [1]
  | otherwise = sum (take 2 prevRow) : nextRow (tail prevRow)
