{-# LANGUAGE OverloadedStrings #-}

module Bob (responseFor) where

import Data.Char (isLetter, isNumber)
import Data.Text (empty, isSuffixOf, strip, toUpper)
import qualified Data.Text as T

responseFor :: T.Text -> T.Text
responseFor call = respond $ T.filter isValidChar call

isValidChar :: Char -> Bool
isValidChar char = isLetter char || isNumber char || char `elem` ['!', '?']

respond :: T.Text -> T.Text
respond call
  | isQuiet = "Fine. Be that way!"
  | isQuestion && isShouting = "Calm down, I know what I'm doing!"
  | isQuestion = "Sure."
  | isShouting = "Whoa, chill out!"
  | otherwise = "Whatever."
  where
    isQuiet = strip call == empty
    isShouting = T.any isLetter call && toUpper call == call
    isQuestion = "?" `isSuffixOf` call
