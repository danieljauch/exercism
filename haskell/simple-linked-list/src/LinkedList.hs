module LinkedList
  ( LinkedList,
    datum,
    fromList,
    isNil,
    new,
    next,
    nil,
    reverseLinkedList,
    toList,
  )
where

data LinkedList a
  = Nil
  | LinkedList {datum :: a, next :: LinkedList a}
  deriving (Eq, Show)

instance Foldable LinkedList where
  foldr _ acc Nil = acc
  foldr f acc (LinkedList d n) = f d $ foldr f acc n

fromList :: [a] -> LinkedList a
fromList = foldr new nil

isNil :: LinkedList a -> Bool
isNil Nil = True
isNil _ = False

new :: a -> LinkedList a -> LinkedList a
new = LinkedList

nil :: LinkedList a
nil = Nil

reverseLinkedList :: LinkedList a -> LinkedList a
reverseLinkedList = foldl (flip new) nil

toList :: LinkedList a -> [a]
toList = foldr (:) []
