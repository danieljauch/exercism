module DNA (toRNA) where

toRNA :: String -> Either Char String
toRNA = traverse mapNucleotide

mapNucleotide :: Char -> Either Char Char
mapNucleotide nucleotide = case nucleotide of
  'A' -> Right 'U'
  'C' -> Right 'G'
  'G' -> Right 'C'
  'T' -> Right 'A'
  invalid -> Left invalid
