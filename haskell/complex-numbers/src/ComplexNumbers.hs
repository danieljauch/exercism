module ComplexNumbers
(Complex,
 conjugate,
 abs,
 exp,
 real,
 imaginary,
 mul,
 add,
 sub,
 div,
 complex) where

import Prelude hiding (div, abs, exp)
import qualified Prelude as Pre

-- Data definition -------------------------------------------------------------
data Complex a = Complex {realPart :: a, imaginaryPart :: a} deriving(Eq, Show)

complex :: (a, a) -> Complex a
complex (r, i) = Complex r i

-- unary operators -------------------------------------------------------------
conjugate :: Num a => Complex a -> Complex a
conjugate (Complex r i) = Complex r (-i)

abs :: Floating a => Complex a -> a
abs (Complex a b) = sqrt $ a^2 + b^2

real :: Num a => Complex a -> a
real = realPart

imaginary :: Num a => Complex a -> a
imaginary = imaginaryPart

exp :: Floating a => Complex a -> Complex a
exp (Complex a' b) = Complex r i
  where
    a = Pre.exp a'
    r = a * cos b
    i = a * sin b

-- binary operators ------------------------------------------------------------
mul :: Num a => Complex a -> Complex a -> Complex a
mul (Complex a b) (Complex c d) = Complex r i
  where
    r = a * c - b * d
    i = b * c + a * d

add :: Num a => Complex a -> Complex a -> Complex a
add (Complex a b) (Complex c d) = Complex (a + c) (b + d)

sub :: Num a => Complex a -> Complex a -> Complex a
sub (Complex a b) (Complex c d) = Complex (a - c) (b - d)

div :: Fractional a => Complex a -> Complex a -> Complex a
div (Complex a b) (Complex c d) = Complex r i
  where
    r = (a * c + b * d) / (c^2 + d^2)
    i = (b * c - a * d) / (c^2 + d^2)
