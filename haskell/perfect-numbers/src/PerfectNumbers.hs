module PerfectNumbers (classify, Classification (..)) where

data Classification = Deficient | Perfect | Abundant deriving (Eq, Show)

classify :: Int -> Maybe Classification
classify x
  | x < 1 = Nothing
  | x == 1 = Just Deficient
  | result > x = Just Abundant
  | result < x = Just Deficient
  | otherwise = Just Perfect
  where
    result = factorSum x

factorSum :: Int -> Int
factorSum = sum . (1 :) . allFactors

allFactors :: Int -> [Int]
allFactors x =
  concatMap (uniqueFactorPair x) $
    filter (isFactor x) (factorRange x)

uniqueFactorPair :: Int -> Int -> [Int]
uniqueFactorPair x y
  | factorPair == y = [y]
  | otherwise = [factorPair, y]
  where
    factorPair = div x y

factorRange :: Int -> [Int]
factorRange n = [2 .. factorMax n]

factorMax :: Int -> Int
factorMax = truncate . sqrt . fromIntegral

isFactor :: Int -> Int -> Bool
isFactor x y = mod x y == 0
