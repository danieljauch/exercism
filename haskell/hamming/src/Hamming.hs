module Hamming (distance) where

distance :: String -> String -> Maybe Int
distance l r
  | length l /= length r = Nothing
  | length l == 0 = Just 0
  | otherwise = Just . length . filter id $ zipWith (/=) l r
