module Series (slices) where

import Data.Char (digitToInt)
import Data.List (tails)

slices :: Int -> String -> [[Int]]
slices size = filter ((== size) . length) . map (take size) . tails . map digitToInt
