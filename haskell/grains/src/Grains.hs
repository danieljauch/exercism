module Grains (square, total) where

square :: Integer -> Maybe Integer
square n
  | n < 1 || n > 64 = Nothing
  | otherwise = Just $ 2 ^ (n -1)

total :: (Num a, Integral a) => a
total = 2 ^ 64 - 1
