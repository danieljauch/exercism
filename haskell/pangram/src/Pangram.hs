module Pangram (isPangram) where

import Data.Char (Char, toLower)
import Data.Set (Set, fromList, intersection, size)

alphabetSet :: Set Char
alphabetSet = fromList ['a' .. 'z']

isPangram :: [Char] -> Bool
isPangram = sizeEqual alphabetSet . intersection alphabetSet . fromList . map toLower

sizeEqual :: Set a -> Set b -> Bool
sizeEqual a b = size a == size b
