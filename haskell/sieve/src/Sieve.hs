module Sieve (primesUpTo) where

-- You should not use any of the division operations when implementing
-- the sieve of Eratosthenes.

import Data.List as List (foldl)
import Data.Map as Map (Map, delete, empty, insert, insertWith, lookup)
import Prelude hiding (div, divMod, mod, quot, quotRem, rem, (/))

primesUpTo :: Integer -> [Integer]
primesUpTo n = sieve [2 .. n]

sieve :: (Ord k, Num k) => [k] -> [k]
sieve xs = sieve' xs Map.empty

sieve' :: (Ord k, Num k) => [k] -> Map k [k] -> [k]
sieve' [] _ = []
sieve' (x : xs) table = case Map.lookup x table of
  Nothing -> x : sieve' xs (Map.insert (x * x) [x] table)
  Just facts -> sieve' xs $ List.foldl reinsert (Map.delete x table) facts
  where
    reinsert table' prime = Map.insertWith (++) (x + prime) [prime] table'
