module PrimeFactors (primeFactors) where

primeFactors :: Integer -> [Integer]
primeFactors = primeFactors' probablePrimes

primeFactors' :: [Integer] -> Integer -> [Integer]
primeFactors' _ 1 = []
primeFactors' ps@(x:xs) n
  | x `divides` n = x : primeFactors' ps (div n x)
  | x * x > n = [n]
  | otherwise = primeFactors' xs n

divides :: Integer -> Integer -> Bool
divides x y = y `rem` x == 0

probablePrimes :: [Integer]
probablePrimes = 2 : [3,5..]
