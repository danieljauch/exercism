module SumOfMultiples (sumOfMultiples) where

sumOfMultiples :: [Int] -> Int -> Int
sumOfMultiples [] _ = 0
sumOfMultiples factors limit
  | 0 `elem` factors = sumOfMultiples (filter (/= 0) factors) limit
  | otherwise = sum [x | x <- [1 .. limit - 1], validMultiple x factors]

validMultiple :: Int -> [Int] -> Bool
validMultiple x factors = any (isFactorOf x) factors

isFactorOf :: Int -> Int -> Bool
isFactorOf x y = mod x y == 0
