module Yacht (yacht, Category (..)) where

import Data.List (group, sort, sortBy)

data Category
  = Ones
  | Twos
  | Threes
  | Fours
  | Fives
  | Sixes
  | FullHouse
  | FourOfAKind
  | LittleStraight
  | BigStraight
  | Choice
  | Yacht
  deriving (Eq)

yacht :: Category -> [Int] -> Int
yacht Ones dice = numReduce 1 dice
yacht Twos dice = numReduce 2 dice
yacht Threes dice = numReduce 3 dice
yacht Fours dice = numReduce 4 dice
yacht Fives dice = numReduce 5 dice
yacht Sixes dice = numReduce 6 dice
yacht FullHouse dice
  | qualifies = sum dice
  | otherwise = 0
  where
    groups = group $ sort dice
    qualifies =
      length groups == 2
        && all (\g -> length g == 2 || length g == 3) groups
yacht FourOfAKind dice
  | qualifies = sum potential
  | otherwise = 0
  where
    potential = take 4 . last . sortBy compare . group . sort $ dice
    qualifies = length potential == 4
yacht LittleStraight dice
  | qualifies = 30
  | otherwise = 0
  where
    qualifies = sort dice == [1..5]
yacht BigStraight dice
  | qualifies = 30
  | otherwise = 0
  where
    qualifies = sort dice == [2..6]
yacht Choice dice = sum dice
yacht Yacht dice
  | qualifies = 50
  | otherwise = 0
  where
    qualifies = (== 5) . length . (!! 0) $ group dice

numReduce :: Int -> [Int] -> Int
numReduce n = (* n) . length . filter (== n)
