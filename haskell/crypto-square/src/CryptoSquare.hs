module CryptoSquare (encode) where

import Data.Char (isAlphaNum, toLower)
import Data.List (transpose)
import Data.List.Split (chunksOf)

encode :: String -> String
encode string = unwords . transpose . chunksOf squareSize $ normal
  where
    normal = map toLower . filter isAlphaNum $ string
    squareSize = ceiling . sqrt . fromIntegral . length $ normal
