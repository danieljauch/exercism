export default class Matrix {
  readonly rows: number[][]
  readonly columns: number[][]

  private _size: number

  constructor(rawInput: string) {
    this.rows = rawInput
      .split("\n")
      .map((rawRow) => rawRow.split(" ").map(Number))
    this._size = this.rows[0].length
    this.columns = rawInput
      .split(/\s/g)
      .map(Number)
      .reduce((acc, n, i) => {
        if (acc.length < this._size) acc.push([n])
        else acc[i % this._size].push(n)
        return acc
      }, [] as number[][])
  }
}
