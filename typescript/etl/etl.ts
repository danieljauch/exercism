interface Legacy {
  [key: string]: string[]
}
interface Score {
  [key: string]: number
}
type ScoreTuple = [string, number]

const normalizeValues = (values: string[], score: number): ScoreTuple[] =>
  values.map((value) => [value.toLowerCase(), score])

const transform = (old: Legacy): Score =>
  Object.entries(old)
    .reduce(
      (acc, [key, values]) => [...acc, ...normalizeValues(values, Number(key))],
      [] as ScoreTuple[]
    )
    .reduce(
      (output, [letter, score]) => ({
        ...output,
        [letter]: score,
      }),
      {} as Score
    )

export default transform
