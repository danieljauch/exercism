export default class SpaceAge {
	seconds: number

	constructor(seconds: number) {
		this.seconds = seconds
	}

	private earthSeconds = (): number => this.seconds / 31557600

	private convert = (factor: number, digits: number = 2) =>
		Number((this.earthSeconds() / factor).toFixed(digits))

	onMercury = (): number => this.convert(0.2408467)
	onVenus = (): number => this.convert(0.61519726)
	onEarth = (): number => this.convert(1)
	onMars = (): number => this.convert(1.8808158)
	onJupiter = (): number => this.convert(11.862615)
	onSaturn = (): number => this.convert(29.447498)
	onUranus = (): number => this.convert(84.016846)
	onNeptune = (): number => this.convert(164.79132)
}
