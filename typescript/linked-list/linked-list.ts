type Maybe<T> = T | undefined

export default class LinkedList<T> {
  private _list?: Node<T>

  count = (): number => this._list?.depth(this._list) || 0

  push = (data: T) => {
    const node = new Node(data)
    this._list ? (this._list.final().next = node) : (this._list = node)
  }

  pop = (): Maybe<T> => {
    const popped = this._list?.final().data
    this._list = this._list?.at(this.count() - 2)
    if (this._list) this._list.next = undefined
    return popped
  }

  shift = (): Maybe<T> => {
    const shifted = this._list?.data
    this._list = this._list?.next
    return shifted
  }

  unshift = (data: T) => {
    const node = new Node(data)
    node.next = this._list
    this._list = node
  }

  private _findNode = (data: T, node: Maybe<Node<T>>): Maybe<Node<T>> => {
    if (!node) return
    return node.data === data ? node : this._findNode(data, node.next)
  }

  private _getPrevious = (data: T, node: Maybe<Node<T>>): Maybe<Node<T>> => {
    if (!node) return
    return node?.next?.data === data ? node : this._findNode(data, node.next)
  }

  delete = (data: T) => {
    const node = this._findNode(data, this._list)
    if (!node) return
    if (node === this._list) {
      this._list = undefined
      return
    }
    const previous = this._getPrevious(node.data, this._list)
    if (!previous) {
      this._list = node.next
      return
    }
    previous.next = node.next
  }
}

class Node<T> {
  data: T
  next?: Node<T>

  constructor(data: T, next?: Node<T>) {
    this.data = data
    this.next = next
  }

  final = (): Node<T> => (this.next ? this.next.final() : this)

  depth = (node?: Node<T>, acc = 0): number =>
    node ? this.depth(node.next, acc + 1) : acc

  at = (depth: number, node: Maybe<Node<T>> = this): Maybe<Node<T>> => {
    if (depth < 0) return
    if (depth === 0) return node
    return this.at(depth - 1, node?.next)
  }
}
