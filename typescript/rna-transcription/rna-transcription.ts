class Transcriptor {
	nucleotides: {[index: string]: string} = {A: "U", C: "G", G: "C", T: "A"}

	public toRna(strand: string): string {
		const rnaStrand = this.createStrand(strand.split(""))

		if (rnaStrand === "Error") throw new Error("Invalid input DNA.")
		return rnaStrand
	}

	private createStrand(
		[head, ...tail]: Array<string>,
		strand: string = ""
	): string {
		const converted = this.nucleotides[head] || "Error"

		if (converted === "Error") return converted

		const updatedStrand = strand + converted

		if (tail.length === 0) return updatedStrand
		return this.createStrand(tail, updatedStrand)
	}
}

export default Transcriptor
