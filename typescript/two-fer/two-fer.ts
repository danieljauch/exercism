class TwoFer {
  public static twoFer( name: string | undefined = "you" ): string {
    return `One for ${name}, one for me.`;
  }
}

export default TwoFer
