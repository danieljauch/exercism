type Kind = "equilateral" | "isosceles" | "scalene"

const allEqual = <T>(array: T[]): boolean =>
  array.reduce<boolean>(
    (isEqual, next, index, sides) =>
      isEqual && (index === 0 || next === sides[index - 1]),
    true
  )

const identity = <T>(item: T): T => item

const groupBy = <T>(
  array: T[],
  comparator: (item: T) => any = identity
): T[][] =>
  Object.values(
    array.reduce((memo, next) => {
      const result = "" + comparator(next)
      if (memo[result]) memo[result].push(next)
      else memo[result] = [next]
      return memo
    }, {} as { [index: string]: T[] })
  )

export default class Triangle {
  sides: number[]

  constructor(...sides: number[]) {
    this.sides = sides.sort((l, r) => l - r)
  }

  private _isInvalid = (): boolean =>
    this.sides.reduce<boolean>(
      (hasInvalid, next) => hasInvalid || next <= 0,
      false
    ) || this.sides[0] + this.sides[1] < this.sides[2]

  kind = (): Kind => {
    if (this._isInvalid()) throw new Error()

    if (allEqual(this.sides)) return "equilateral"
    if (groupBy(this.sides).length === 2) return "isosceles"
    return "scalene"
  }
}
