export default class BinarySearch {
  array?: number[]

  constructor(array: number[]) {
    if (this._validArray(array)) this.array = array
  }

  private _validArray = (array: number[]): boolean =>
    array.reduce<boolean>(
      (isValid, curr, index) =>
        isValid ? index === 0 || curr >= array[index - 1] : false,
      true
    )

  indexOf = (
    n: number,
    array = this.array,
    start = 0,
    end = array?.length
  ): number => {
    if (!array?.length || !end) return -1
    const mid = Math.floor((end - start) / 2)
    if (array[mid - start] === n) return mid + start
    if (array[mid - start] > n)
      return this.indexOf(n, array.slice(0, mid), start, mid)
    return this.indexOf(n, array.slice(mid), mid, end)
  }
}
