export default class Pangram {
	pangram: string

	constructor(pangram: string) {
		this.pangram = pangram
	}

	isPangram = (): boolean =>
		new Set(this.pangram.toLowerCase().replace(/[^a-z]/i, "")).size === 26
}
