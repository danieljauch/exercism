export default class RobotName {
	name: string
	private registry: Set<string>

	constructor() {
		this.registry = new Set()
		this.name = this.getName()
	}

	private getName = (): string => {
		let newName = this.makeName()
		while (this.registry.has(newName)) newName = this.makeName()
		this.registry.add(newName)
		return newName
	}

	public resetName = (): void => {
		this.name = this.getName()
	}

	private makeName = (): string =>
		"".concat(this.getLetter(2), this.getNumber(3))

	private getLetter = (count: number): string =>
		this.getRandomCharacters(count, "ABCDEFGHIJKLMNOPQRSTUVWXYZ")

	private getNumber = (count: number): string =>
		this.getRandomCharacters(count, "1234567890")

	private getRandomCharacters = (
		count: number,
		str: string,
		acc: string = ""
	): string =>
		count
			? this.getRandomCharacters(
					count - 1,
					str,
					acc + this.randomCharacterFromString(str)
				)
			: acc

	private randomCharacterFromString = (str: string) =>
		str.substr(Math.floor(Math.random() * str.length), 1)
}
