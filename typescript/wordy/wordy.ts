export const ArgumentError = new Error("that's not right")

type Operation = "plus" | "minus" | "multiplied by" | "divided by"
type Part = number | Operation
type MathOp = (l: number, r: number) => number
interface Stack {
  number: number
  operation?: Operation
}

const OP_MAP: { [index: string]: MathOp } = {
  plus: (l, r) => l + r,
  minus: (l, r) => l - r,
  "multiplied by": (l, r) => l * r,
  "divided by": (l, r) => l / r,
}

const isArgument = (part: Part): part is number => typeof part === "number"

const numeral = "-?\\d+"
const operation = "plus|minus|multiplied by|divided by"
const validation = new RegExp(
  `^(What is )${numeral}( (${operation}) ${numeral})+\\?$`
)
const match = new RegExp(`(${numeral}|${operation})`, "g")

export class WordProblem {
  private _parserResult: Part[]

  constructor(question: string) {
    this._parserResult = this._parse(question)
  }

  private _parse = (question: string): Part[] =>
    validation.exec(question)
      ? [...question.matchAll(match)].map<Part>(([capture]) =>
          isNaN(Number(capture)) ? (capture as Operation) : Number(capture)
        )
      : []

  private _doOp = (operation: Operation): MathOp => OP_MAP[operation]

  answer = (): number =>
    this._parserResult?.reduce<Stack>(
      (acc, part): Stack => {
        if (!acc.number && isArgument(part)) return { number: part }
        if (isArgument(part) && acc?.operation)
          return { number: this._doOp(acc.operation)(acc.number, part) }
        if (!isArgument(part)) return { number: acc.number, operation: part }
        return acc
      },
      { number: 0 } as Stack
    ).number || 0
}
