type Roster = Map<string, string[]>

export default class GradeSchool {
  private _roster: Roster

  constructor() {
    this._roster = new Map() as Roster
  }

  studentRoster = (): Roster => {
    const copy = new Map() as Roster
    this._roster.forEach((students, grade) => copy.set(grade, [...students]))
    return copy
  }

  private _safeGet = (grade: number): string[] => [
    ...(this._roster.get(grade.toString()) || []),
  ]

  addStudent = (name: string, grade: number) =>
    this._roster.set(grade.toString(), [...this._safeGet(grade), name].sort())

  studentsInGrade = (grade: number): string[] => [...this._safeGet(grade)]
}
