const cons = <T>(arr: T[], prefix: T): T[] => [prefix, ...arr]

class ReverseString {
  static reverse = (string: string): string =>
    [...string].reduce<string[]>(cons, [] as string[]).join("")
}

export default ReverseString
