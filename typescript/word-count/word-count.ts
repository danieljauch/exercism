type WordMap = Map<string, number>

export default class Words {
  count = (words: string): WordMap =>
    this.normalizeInput(words)
      .reduce(
        (acc: WordMap, nextWord: string): WordMap =>
          acc.set(nextWord, this.getCountWithDefault(acc, nextWord) + 1),
        new Map()
      )

  private normalizeInput = (raw: string): string[] =>
    raw.trim().toLowerCase().split(/[\s]+/)

  private getCountWithDefault = (
    map: WordMap,
    key: string,
    fallback: number = 0
  ): number => map.get(key) || fallback
}
