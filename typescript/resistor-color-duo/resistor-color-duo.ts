const COLORS = [
  "black",
  "brown",
  "red",
  "orange",
  "yellow",
  "green",
  "blue",
  "violet",
  "grey",
  "white",
]

export class ResistorColor {
  private colors: string[]

  constructor(colors: string[]) {
    if (colors.length < 2)
      throw new Error("At least two colors need to be present")
    const [first, second] = colors
    this.colors = [first, second]
  }

  value = (): number =>
    Number(this.colors.reduce((sum, color) => sum + COLORS.indexOf(color), ""))
}
