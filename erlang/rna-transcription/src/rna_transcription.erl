-module(rna_transcription).
-export([to_rna/1]).
-define(Nucleotides, #{"A" => "U", "C" => "G", "G" => "C", "T" => "A", }).

-spec to_rna(string()) -> string().
to_rna(Strand) ->
  lists:map(fun(Nucleotide) -> maps:get(Nucleotide, ?Nucleotides, "") end, Strand).
