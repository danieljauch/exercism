-module(collatz_conjecture).

-export([steps/1]).

-spec steps(integer()) -> integer().

steps(N) when N < 1 -> error(badarg);
steps(N) -> do_steps(N, 0).

-spec is_power_of_two(integer()) -> boolean().

is_power_of_two(N) ->
    math:log2(N) == math:floor(math:log2(N)).

-spec do_steps(integer(), integer()) -> integer().

do_steps(1, Count) -> Count;
do_steps(N, Count) ->
    if is_power_of_two(N) -> Count + math:log2(N);
       true -> do_steps(next(N), Count + 1)
    end.

-spec next(integer()) -> integer().

next(N) when N rem 2 =:= 0 -> N div 2;
next(N) -> 3 * N + 1.
