-module(raindrops).
-export([convert/1]).
-define(Rules, [{3, "Pling"}, {5, "Plang"}, {7, "Plong"}]).

-spec convert(integer()) -> string().
convert(Number) ->
  lists:flatten(sound_off(Number)).

-spec sound_off(integer()) -> string().
sound_off(Number) ->
  case [Sound || {Factor, Sound} <- ?Rules, Number rem Factor =:= 0] of
    [] -> integer_to_list(Number);
    Sounds -> Sounds
  end.
