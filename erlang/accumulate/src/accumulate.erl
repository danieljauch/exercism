-module(accumulate).

-export([accumulate/2]).

-spec accumulate(fun((A) -> B), [A]) -> [B].

accumulate(Func, List) -> [Func(L) || L <- List].
