-module(two_fer).

-export([two_fer/0, two_fer/1]).

-spec two_fer() -> string().
two_fer() ->
  two_fer("you").

-spec two_fer(string()) -> string().
two_fer(Name) ->
  "One for " ++ Name ++ ", one for me.".
